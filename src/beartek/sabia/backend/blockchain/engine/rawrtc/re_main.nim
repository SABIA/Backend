## *
##  @file re_main.h  Interface to main polling routine
##
##  Copyright (C) 2010 Creytiv.com
##

const
  FD_READ* = 1 shl 0
  FD_WRITE* = 1 shl 1
  FD_EXCEPT* = 1 shl 2

## *
##  File descriptor event handler
##
##  @param flags  Event flags
##  @param arg    Handler argument
##

type
  fd_h* = proc (flags: cint; arg: pointer)

## *
##  Thread-safe signal handler
##
##  @param sig Signal number
##

type
  re_signal_h* = proc (sig: cint)

proc fd_listen*(fd: cint; flags: cint; fh: ptr fd_h; arg: pointer): cint {.
    importc: "fd_listen", dynlib: "librawrtc.so".}
proc fd_close*(fd: cint) {.importc: "fd_close", dynlib: "librawrtc.so".}
proc fd_setsize*(maxfds: cint): cint {.importc: "fd_setsize", dynlib: "librawrtc.so".}
proc fd_debug*() {.importc: "fd_debug", dynlib: "librawrtc.so".}
proc libre_init*(): cint {.importc: "libre_init", dynlib: "librawrtc.so".}
proc libre_close*() {.importc: "libre_close", dynlib: "librawrtc.so".}
proc re_main*(signalh: re_signal_h): cint {.importc: "re_main",
    dynlib: "librawrtc.so".}
proc re_cancel*() {.importc: "re_cancel", dynlib: "librawrtc.so".}
#proc re_debug*(pf: ptr re_printf; unused: pointer): cint {.importc: "re_debug",
#    dynlib: "librawrtc.so".}
proc re_thread_init*(): cint {.importc: "re_thread_init", dynlib: "librawrtc.so".}
proc re_thread_close*() {.importc: "re_thread_close", dynlib: "librawrtc.so".}
proc re_thread_enter*() {.importc: "re_thread_enter", dynlib: "librawrtc.so".}
proc re_thread_leave*() {.importc: "re_thread_leave", dynlib: "librawrtc.so".}
proc re_set_mutex*(mutexp: pointer) {.importc: "re_set_mutex", dynlib: "librawrtc.so".}
## * Polling methods

type
  poll_method* {.size: sizeof(cint).} = enum
    METHOD_NULL = 0, METHOD_POLL, METHOD_SELECT, METHOD_EPOLL, METHOD_KQUEUE, ##  sep
    METHOD_MAX


proc poll_method_set*(`method`: poll_method): cint {.importc: "poll_method_set",
    dynlib: "librawrtc.so".}
proc poll_method_best*(): poll_method {.importc: "poll_method_best",
                                     dynlib: "librawrtc.so".}
proc poll_method_name*(`method`: poll_method): cstring {.
    importc: "poll_method_name", dynlib: "librawrtc.so".}
#proc poll_method_type*(`method`: ptr poll_method; name: ptr pl): cint {.
#    importc: "poll_method_type", dynlib: "librawrtc.so".}
