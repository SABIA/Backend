##  TODO: Make this a build configuration
import openssl
import re_mbuf

const
  RAWRTC_DEBUG* = 1

{.deadCodeElim: on.}
when defined(windows):
  const
    librawrtc* = "librawrtc.dll"
elif defined(macosx):
  const
    librawrtc* = "librawrtc.dylib"
else:
  const
    librawrtc* = "librawrtc.so"
## #define ZF_LOG_LIBRARY_PREFIX rawrtc_
## #ifdef RAWRTC_DEBUG
##     #define RAWRTC_ZF_LOG_LEVEL ZF_LOG_DEBUG
## #else
##     #define RAWRTC_ZF_LOG_LEVEL ZF_LOG_WARN
## #endif
## #include <zf_log.h>

const
  RAWRTC_DEBUG_LEVEL* = 5
  HAVE_INTTYPES_H* = true

##
##  Version
##
##  Follows Semantic Versioning 2.0.0,
##  see: https://semver.org
##
##  TODO: Find a way to keep this in sync with the one in CMakeLists.txt
##

const
  RAWRTC_VERSION* = "0.2.1"

##
##  Return codes.
##

type
  rawrtc_code* {.size: sizeof(cint).} = enum
    RAWRTC_CODE_UNKNOWN_ERROR = -2, RAWRTC_CODE_NOT_IMPLEMENTED = -1,
    RAWRTC_CODE_SUCCESS = 0, RAWRTC_CODE_INITIALISE_FAIL,
    RAWRTC_CODE_INVALID_ARGUMENT, RAWRTC_CODE_NO_MEMORY,
    RAWRTC_CODE_INVALID_STATE, RAWRTC_CODE_UNSUPPORTED_PROTOCOL,
    RAWRTC_CODE_UNSUPPORTED_ALGORITHM, RAWRTC_CODE_NO_VALUE,
    RAWRTC_CODE_NO_SOCKET, RAWRTC_CODE_INVALID_CERTIFICATE,
    RAWRTC_CODE_INVALID_FINGERPRINT, RAWRTC_CODE_INSUFFICIENT_SPACE,
    RAWRTC_CODE_STILL_IN_USE, RAWRTC_CODE_INVALID_MESSAGE,
    RAWRTC_CODE_MESSAGE_TOO_LONG, RAWRTC_CODE_TRY_AGAIN_LATER,
    RAWRTC_CODE_STOP_ITERATION, RAWRTC_CODE_NOT_PERMITTED


##  IMPORTANT: Add translations for new return codes in `utils.c`!
##
##  Certificate private key types.
##

type
  rawrtc_certificate_key_type* {.size: sizeof(cint).} = enum
    RAWRTC_CERTIFICATE_KEY_TYPE_RSA ,#= TLS_KEYTYPE_RSA,
    RAWRTC_CERTIFICATE_KEY_TYPE_EC #= TLS_KEYTYPE_EC


##
##  Certificate signing hash algorithms.
##

type
  rawrtc_certificate_sign_algorithm* {.size: sizeof(cint).} = enum
    RAWRTC_CERTIFICATE_SIGN_ALGORITHM_NONE = 0,
    RAWRTC_CERTIFICATE_SIGN_ALGORITHM_SHA256 ,#= TLS_FINGERPRINT_SHA256,
    RAWRTC_CERTIFICATE_SIGN_ALGORITHM_SHA384,
    RAWRTC_CERTIFICATE_SIGN_ALGORITHM_SHA512


##
##  Certificate encoding.
##

type
  rawrtc_certificate_encode* {.size: sizeof(cint).} = enum
    RAWRTC_CERTIFICATE_ENCODE_CERTIFICATE, RAWRTC_CERTIFICATE_ENCODE_PRIVATE_KEY,
    RAWRTC_CERTIFICATE_ENCODE_BOTH


##
##  SDP type.
##

type
  rawrtc_sdp_type* {.size: sizeof(cint).} = enum
    RAWRTC_SDP_TYPE_OFFER, RAWRTC_SDP_TYPE_PROVISIONAL_ANSWER,
    RAWRTC_SDP_TYPE_ANSWER, RAWRTC_SDP_TYPE_ROLLBACK


##
##  ICE gather policy.
##

type
  rawrtc_ice_gather_policy* {.size: sizeof(cint).} = enum
    RAWRTC_ICE_GATHER_POLICY_ALL, RAWRTC_ICE_GATHER_POLICY_NOHOST,
    RAWRTC_ICE_GATHER_POLICY_RELAY


##
##  ICE credential type
##

type
  rawrtc_ice_credential_type* {.size: sizeof(cint).} = enum
    RAWRTC_ICE_CREDENTIAL_TYPE_NONE, RAWRTC_ICE_CREDENTIAL_TYPE_PASSWORD,
    RAWRTC_ICE_CREDENTIAL_TYPE_TOKEN


##
##  ICE gatherer state.
##

type
  rawrtc_ice_gatherer_state* {.size: sizeof(cint).} = enum
    RAWRTC_ICE_GATHERER_STATE_NEW, RAWRTC_ICE_GATHERER_STATE_GATHERING,
    RAWRTC_ICE_GATHERER_STATE_COMPLETE, RAWRTC_ICE_GATHERER_STATE_CLOSED


##
##  ICE component.
##  Note: For now, only "RTP" will be supported/returned as we do not support
##  RTP or RTCP.
##

type
  rawrtc_ice_component* {.size: sizeof(cint).} = enum
    RAWRTC_ICE_COMPONENT_RTP, RAWRTC_ICE_COMPONENT_RTCP


##
##  ICE role.
##

type
  rawrtc_ice_role* {.size: sizeof(cint).} = enum
    RAWRTC_ICE_ROLE_UNKNOWN ,#= ICE_ROLE_UNKNOWN,
    RAWRTC_ICE_ROLE_CONTROLLING ,#= ICE_ROLE_CONTROLLING,
    RAWRTC_ICE_ROLE_CONTROLLED ,#= ICE_ROLE_CONTROLLED


##
##  ICE transport state.
##

type
  rawrtc_ice_transport_state* {.size: sizeof(cint).} = enum
    RAWRTC_ICE_TRANSPORT_STATE_NEW, RAWRTC_ICE_TRANSPORT_STATE_CHECKING,
    RAWRTC_ICE_TRANSPORT_STATE_CONNECTED, RAWRTC_ICE_TRANSPORT_STATE_COMPLETED,
    RAWRTC_ICE_TRANSPORT_STATE_DISCONNECTED, RAWRTC_ICE_TRANSPORT_STATE_FAILED,
    RAWRTC_ICE_TRANSPORT_STATE_CLOSED


##
##  DTLS role.
##

type
  rawrtc_dtls_role* {.size: sizeof(cint).} = enum
    RAWRTC_DTLS_ROLE_AUTO, RAWRTC_DTLS_ROLE_CLIENT, RAWRTC_DTLS_ROLE_SERVER


##
##  DTLS transport state.
##

type
  rawrtc_dtls_transport_state* {.size: sizeof(cint).} = enum
    RAWRTC_DTLS_TRANSPORT_STATE_NEW, RAWRTC_DTLS_TRANSPORT_STATE_CONNECTING,
    RAWRTC_DTLS_TRANSPORT_STATE_CONNECTED, RAWRTC_DTLS_TRANSPORT_STATE_CLOSED,
    RAWRTC_DTLS_TRANSPORT_STATE_FAILED


##
##  Data channel is unordered bit flag.
##

const
  RAWRTC_DATA_CHANNEL_TYPE_IS_UNORDERED* = 0x00000080

##
##  Data channel types.
##

type
  rawrtc_data_channel_type* {.size: sizeof(cint).} = enum
    RAWRTC_DATA_CHANNEL_TYPE_RELIABLE_ORDERED = 0x00000000,
    RAWRTC_DATA_CHANNEL_TYPE_UNRELIABLE_ORDERED_RETRANSMIT = 0x00000001,
    RAWRTC_DATA_CHANNEL_TYPE_UNRELIABLE_ORDERED_TIMED = 0x00000002,
    RAWRTC_DATA_CHANNEL_TYPE_RELIABLE_UNORDERED = 0x00000080,
    RAWRTC_DATA_CHANNEL_TYPE_UNRELIABLE_UNORDERED_RETRANSMIT = 0x00000081,
    RAWRTC_DATA_CHANNEL_TYPE_UNRELIABLE_UNORDERED_TIMED = 0x00000082


##  IMPORTANT: If you add a new type, ensure that every data channel transport handles it
##             correctly! Also, ensure this still works with the unordered bit flag above or
##             update the implementations.
##
##  Data channel message flags.
##

type
  rawrtc_data_channel_message_flag* {.size: sizeof(cint).} = enum
    RAWRTC_DATA_CHANNEL_MESSAGE_FLAG_NONE = 1 shl 0,
    RAWRTC_DATA_CHANNEL_MESSAGE_FLAG_IS_ABORTED = 1 shl 1,
    RAWRTC_DATA_CHANNEL_MESSAGE_FLAG_IS_COMPLETE = 1 shl 2,
    RAWRTC_DATA_CHANNEL_MESSAGE_FLAG_IS_BINARY = 1 shl 3


##
##  SCTP transport state.
##

type
  rawrtc_sctp_transport_state* {.size: sizeof(cint).} = enum
    RAWRTC_SCTP_TRANSPORT_STATE_NEW, RAWRTC_SCTP_TRANSPORT_STATE_CONNECTING,
    RAWRTC_SCTP_TRANSPORT_STATE_CONNECTED, RAWRTC_SCTP_TRANSPORT_STATE_CLOSED


##
##  ICE protocol.
##

type
  rawrtc_ice_protocol* {.size: sizeof(cint).} = enum
    RAWRTC_ICE_PROTOCOL_UDP #[= IPPROTO_UDP]#, RAWRTC_ICE_PROTOCOL_TCP #[= IPPROTO_TCP]#


##
##  ICE candidate type.
##

type
  rawrtc_ice_candidate_type* {.size: sizeof(cint).} = enum
    RAWRTC_ICE_CANDIDATE_TYPE_HOST ,#= ICE_CAND_TYPE_HOST,
    RAWRTC_ICE_CANDIDATE_TYPE_SRFLX ,#= ICE_CAND_TYPE_SRFLX,
    RAWRTC_ICE_CANDIDATE_TYPE_PRFLX ,#= ICE_CAND_TYPE_PRFLX,
    RAWRTC_ICE_CANDIDATE_TYPE_RELAY #= ICE_CAND_TYPE_RELAY


##
##  ICE TCP candidate type.
##

type
  rawrtc_ice_tcp_candidate_type* {.size: sizeof(cint).} = enum
    RAWRTC_ICE_TCP_CANDIDATE_TYPE_ACTIVE ,#= ICE_TCP_ACTIVE,
    RAWRTC_ICE_TCP_CANDIDATE_TYPE_PASSIVE ,#= ICE_TCP_PASSIVE,
    RAWRTC_ICE_TCP_CANDIDATE_TYPE_SO #= ICE_TCP_SO


##
##  Data channel state.
##

type
  rawrtc_data_channel_state* {.size: sizeof(cint).} = enum
    RAWRTC_DATA_CHANNEL_STATE_CONNECTING, RAWRTC_DATA_CHANNEL_STATE_OPEN,
    RAWRTC_DATA_CHANNEL_STATE_CLOSING, RAWRTC_DATA_CHANNEL_STATE_CLOSED


##
##  Signalling state.
##

type
  rawrtc_signaling_state* {.size: sizeof(cint).} = enum
    RAWRTC_SIGNALING_STATE_STABLE, RAWRTC_SIGNALING_STATE_HAVE_LOCAL_OFFER,
    RAWRTC_SIGNALING_STATE_HAVE_REMOTE_OFFER,
    RAWRTC_SIGNALING_STATE_HAVE_LOCAL_PROVISIONAL_ANSWER,
    RAWRTC_SIGNALING_STATE_HAVE_REMOTE_PROVISIONAL_ANSWER,
    RAWRTC_SIGNALING_STATE_CLOSED


##
##  Peer connection state.
##

type
  rawrtc_peer_connection_state* {.size: sizeof(cint).} = enum
    RAWRTC_PEER_CONNECTION_STATE_NEW, RAWRTC_PEER_CONNECTION_STATE_CONNECTING,
    RAWRTC_PEER_CONNECTION_STATE_CONNECTED,
    RAWRTC_PEER_CONNECTION_STATE_DISCONNECTED,
    RAWRTC_PEER_CONNECTION_STATE_FAILED, RAWRTC_PEER_CONNECTION_STATE_CLOSED


when defined(SCTP_REDIRECT_TRANSPORT):
  ##
  ##  SCTP redirect transport states.
  ##  TODO: Private -> sctp_redirect_transport.h
  ##
  type
    rawrtc_sctp_redirect_transport_state* {.size: sizeof(cint).} = enum
      RAWRTC_SCTP_REDIRECT_TRANSPORT_STATE_NEW,
      RAWRTC_SCTP_REDIRECT_TRANSPORT_STATE_OPEN,
      RAWRTC_SCTP_REDIRECT_TRANSPORT_STATE_CLOSED
##
##  Data transport type.
##  TODO: private -> data_transport.h
##

type
  rawrtc_data_transport_type* {.size: sizeof(cint).} = enum
    RAWRTC_DATA_TRANSPORT_TYPE_SCTP


##
##  ICE candidate storage type (internal).
##  TODO: Private
##

type
  rawrtc_ice_candidate_storage* {.size: sizeof(cint).} = enum
    RAWRTC_ICE_CANDIDATE_STORAGE_RAW, RAWRTC_ICE_CANDIDATE_STORAGE_LCAND,
    RAWRTC_ICE_CANDIDATE_STORAGE_RCAND


##
##  ICE server type.
##  Note: Update `ice_server_schemes` if changed.
##  TODO: private -> ice_gatherer.h
##

type
  rawrtc_ice_server_type* {.size: sizeof(cint).} = enum
    RAWRTC_ICE_SERVER_TYPE_STUN, RAWRTC_ICE_SERVER_TYPE_TURN


##
##  ICE server transport protocol.
##  TODO: private -> ice_gatherer.h
##

type
  rawrtc_ice_server_transport* {.size: sizeof(cint).} = enum
    RAWRTC_ICE_SERVER_TRANSPORT_UDP, RAWRTC_ICE_SERVER_TRANSPORT_TCP,
    RAWRTC_ICE_SERVER_TRANSPORT_DTLS, RAWRTC_ICE_SERVER_TRANSPORT_TLS


##
##  Length of various arrays.
##  TODO: private
##

const
  ICE_USERNAME_FRAGMENT_LENGTH* = 16
  ICE_PASSWORD_LENGTH* = 32
  DTLS_ID_LENGTH* = 32

##
##  Struct prototypes.
##  TODO: Remove
##

type
  rawrtc_ice_server_url_context* {.bycopy.} = object

  #rawrtc_ice_candidate* {.bycopy.} = object

  rawrtc_data_channel* {.bycopy.} = object

  rawrtc_dtls_transport* {.bycopy.} = object

  #rawrtc_dtls_parameters* {.bycopy.} = object

  rawrtc_data_channel_parameters* {.bycopy.} = object

  rawrtc_data_transport* {.bycopy.} = object

  rawrtc_sctp_transport* {.bycopy.} = object

  #rawrtc_sctp_capabilities* {.bycopy.} = object

  rawrtc_peer_connection_ice_candidate* {.bycopy.} = object


##
##  Raw ICE candidate (pending candidate).
##  TODO: private
##

type
  rawrtc_ice_candidate_raw* {.bycopy.} = object
    #foundation*: cstring       ##  copied
    priority*: uint32
    ip*: cstring               ##  copied
    protocol*: rawrtc_ice_protocol
    port*: uint16
    `type`*: rawrtc_ice_candidate_type
    tcp_type*: rawrtc_ice_tcp_candidate_type
    #related_address*: cstring  ##  copied, nullable
    related_port*: uint16


##
##  ICE candidate.
##  TODO: private
##

type
  INNER_C_UNION_2499034602* {.bycopy.} = object {.union.}
    raw_candidate*: ptr rawrtc_ice_candidate_raw
    #local_candidate*: ptr ice_lcand
    #remote_candidate*: ptr ice_rcand

  rawrtc_ice_candidate* {.bycopy.} = object
    storage_type*: rawrtc_ice_candidate_storage
    candidate*: INNER_C_UNION_2499034602



##
##  ICE gatherer state change handler.
##

type
  rawrtc_ice_gatherer_state_change_handler* = proc (
      state: rawrtc_ice_gatherer_state; arg: pointer) {.cdecl.} ##  read-only

##
##  ICE gatherer error handler.
##

type
  rawrtc_ice_gatherer_error_handler* = proc (candidate: ptr rawrtc_ice_candidate;
      url: cstring; error_code: uint16; error_text: cstring; arg: pointer) {.cdecl.} ##  read-only, nullable
                                                                              ##  read-only
                                                                              ##  read-only
                                                                              ##  read-only

##
##  ICE gatherer local candidate handler.
##  Note: 'candidate' and 'url' will be NULL in case gathering is complete.
##  'url' will be NULL in case a host candidate has been gathered.
##

type
  rawrtc_ice_gatherer_local_candidate_handler* = proc (
      candidate: ptr rawrtc_ice_candidate; url: cstring; arg: pointer) {.cdecl.} ##  read-only

##
##  ICE transport state change handler.
##

type
  rawrtc_ice_transport_state_change_handler* = proc (
      state: rawrtc_ice_transport_state; arg: pointer) {.cdecl.}

##
##  ICE transport pair change handler.
##

type
  rawrtc_ice_transport_candidate_pair_change_handler* = proc (
      local: ptr rawrtc_ice_candidate; remote: ptr rawrtc_ice_candidate; arg: pointer) {.
      cdecl.}                 ##  read-only
             ##  read-only

##
##  DTLS transport state change handler.
##

type
  rawrtc_dtls_transport_state_change_handler* = proc (
      state: rawrtc_dtls_transport_state; arg: pointer) {.cdecl.}

##
##  DTLS transport error handler.
##

type
  rawrtc_dtls_transport_error_handler* = proc (arg: pointer) {.cdecl.} ##  TODO: error.message (probably from OpenSSL)

##
##  SCTP transport state change handler.
##

type
  rawrtc_sctp_transport_state_change_handler* = proc (
      state: rawrtc_sctp_transport_state; arg: pointer) {.cdecl.}

##
##  Data channel open handler.
##

type
  rawrtc_data_channel_open_handler* = proc (arg: pointer) {.cdecl.}

##
##  Data channel buffered amount low handler.
##

type
  rawrtc_data_channel_buffered_amount_low_handler* = proc (arg: pointer) {.cdecl.}

##
##  Data channel error handler.
##

type
  rawrtc_data_channel_error_handler* = proc (arg: pointer) {.cdecl.} ##  TODO

##
##  Data channel close handler.
##

type
  rawrtc_data_channel_close_handler* = proc (arg: pointer) {.cdecl.}

##
##  Data channel message handler.
##
##  Note: `buffer` may be NULL in case partial delivery has been
##        requested and a message has been aborted (this can only happen
##        on partially reliable channels).
##
##  TODO: ORTC is really unclear about that handler. Consider improving it with a PR.
##

type
  rawrtc_data_channel_message_handler* = proc (buffer: ptr mbuf;
      flags: rawrtc_data_channel_message_flag; arg: pointer) {.cdecl.} ##  nullable (in case partial delivery has been requested)

##
##  Data channel handler.
##
##  You should call `rawrtc_data_channel_set_options` in this handler
##  before doing anything else if you want to change behaviour of the
##  data channel.
##

type
  rawrtc_data_channel_handler* = proc (data_channel: ptr rawrtc_data_channel;
                                    arg: pointer) {.cdecl.} ##  read-only, MUST be referenced when used

##
##  Peer connection state change handler.
##

type
  rawrtc_peer_connection_state_change_handler* = proc (
      state: rawrtc_peer_connection_state; arg: pointer) {.cdecl.} ##  read-only

##
##  Negotiation needed handler.
##

type
  rawrtc_negotiation_needed_handler* = proc (arg: pointer) {.cdecl.}

##
##  Peer connection ICE local candidate handler.
##  Note: 'candidate' and 'url' will be NULL in case gathering is complete.
##  'url' will be NULL in case a host candidate has been gathered.
##

type
  rawrtc_peer_connection_local_candidate_handler* = proc (
      candidate: ptr rawrtc_peer_connection_ice_candidate; url: cstring; arg: pointer) {.
      cdecl.}                 ##  read-only

##
##  Peer connection ICE local candidate error handler.
##  Note: 'candidate' and 'url' will be NULL in case gathering is complete.
##  'url' will be NULL in case a host candidate has been gathered.
##

type
  rawrtc_peer_connection_local_candidate_error_handler* = proc (
      candidate: ptr rawrtc_peer_connection_ice_candidate; url: cstring;
      error_code: uint16; error_text: cstring; arg: pointer) {.cdecl.} ##  read-only, nullable
                                                                  ##  read-only
                                                                  ##  read-only
                                                                  ##  read-only

##
##  Signaling state handler.
##

type
  rawrtc_signaling_state_change_handler* = proc (state: rawrtc_signaling_state;
      arg: pointer) {.cdecl.}   ##  read-only

##
##  Handle incoming data messages.
##  TODO: private -> dtls_transport.h
##

type
  rawrtc_dtls_transport_receive_handler* = proc (buffer: ptr mbuf; arg: pointer) {.cdecl.}

##
##  Create the data channel (transport handler).
##  TODO: private -> data_transport.h
##

type
  rawrtc_data_transport_channel_create_handler* = proc (
      transport: ptr rawrtc_data_transport; channel: ptr rawrtc_data_channel; parameters: ptr rawrtc_data_channel_parameters): rawrtc_code {.
      cdecl.}                 ##  referenced
  ##  read-only


##
##  Close the data channel (transport handler).
##  TODO: private -> data_transport.h
##

type
  rawrtc_data_transport_channel_close_handler* = proc (
      channel: ptr rawrtc_data_channel): rawrtc_code {.cdecl.}


##
##  Send data via the data channel (transport handler).
##  TODO: private -> data_transport.h
##

type
  rawrtc_data_transport_channel_send_handler* = proc (
      channel: ptr rawrtc_data_channel; buffer: ptr mbuf; is_binary: bool): rawrtc_code {.
      cdecl.}                 ##  nullable (if size 0), referenced


##
##  Configuration.
##  TODO: Add to a constructor... somewhere
##

type
  rawrtc_config* {.bycopy.} = object
    pacing_interval*: uint32
    ipv4_enable*: bool
    ipv6_enable*: bool
    udp_enable*: bool
    tcp_enable*: bool
    sign_algorithm*: rawrtc_certificate_sign_algorithm
    ice_server_normal_transport*: rawrtc_ice_server_transport
    ice_server_secure_transport*: rawrtc_ice_server_transport
    stun_keepalive_interval*: uint32
    #stun_config*: stun_conf


##
##  Message buffer.
##  TODO: private
##

type
  rawrtc_buffered_message* {.bycopy.} = object
    #le*: le
    buffer*: ptr mbuf           ##  referenced
    context*: pointer          ##  referenced, nullable


##
##  Certificate options.
##  TODO: private
##

type
  rawrtc_certificate_options* {.bycopy.} = object
    key_type*: rawrtc_certificate_key_type
    common_name*: cstring      ##  copied
    valid_until*: uint32
    sign_algorithm*: rawrtc_certificate_sign_algorithm
    named_curve*: cstring      ##  nullable, copied, ignored for RSA
    modulus_length*: uint32 ##  ignored for ECC


##
##  Certificate.
##  TODO: private
##

type
  rawrtc_certificate* {.bycopy.} = object
    #le*: le
    certificate*: ptr PX509
    key*: ptr EVP_PKEY
    key_type*: rawrtc_certificate_key_type


##
##  ICE gather options.
##  TODO: private
##

type
  rawrtc_ice_gather_options* {.bycopy.} = object
    gather_policy*: rawrtc_ice_gather_policy
    #ice_servers*: list


##
##  ICE server.
##  TODO: private
##

type
  rawrtc_ice_server* {.bycopy.} = object
    #le*: le
    #urls*: list                ##  deep-copied
    username*: cstring         ##  copied
    credential*: cstring       ##  copied
    credential_type*: rawrtc_ice_credential_type


##
##  ICE server URL DNS resolve context.
##  TODO: private -> ice_gatherer.h
##

type
  rawrtc_ice_server_url_dns_context* {.bycopy.} = object
    dns_type*: uint16
    #url*: ptr rawrtc_ice_server_url
    #gatherer*: ptr rawrtc_ice_gatherer
    #dns_query*: ptr dns_query


##
##  ICE server URL. (list element)
##  TODO: private
##

type
  rawrtc_ice_server_url* {.bycopy.} = object
    #le*: le
    url*: cstring              ##  copied
    #host*: pl                  ##  points inside `url`
    `type`*: rawrtc_ice_server_type
    transport*: rawrtc_ice_server_transport
    #ipv4_address*: sa
    dns_a_context*: ptr rawrtc_ice_server_url_dns_context
    #ipv6_address*: sa
    dns_aaaa_context*: ptr rawrtc_ice_server_url_dns_context

##
##  ICE parameters.
##  TODO: private
##

type
  rawrtc_ice_parameters* {.bycopy.} = object
    username_fragment*: cstring ##  copied
    password*: cstring         ##  copied
    ice_lite*: bool


##
##  ICE gatherer.
##  TODO: private
##

type
  rawrtc_ice_gatherer* {.bycopy.} = object
    state*: rawrtc_ice_gatherer_state
    options*: ptr rawrtc_ice_gather_options ##  referenced
    state_change_handler*: ptr rawrtc_ice_gatherer_state_change_handler ##  nullable
    error_handler*: ptr rawrtc_ice_gatherer_error_handler ##  nullable
    local_candidate_handler*: ptr rawrtc_ice_gatherer_local_candidate_handler ##  nullable
    arg*: pointer              ##  nullable
    #buffered_messages*: list   ##  TODO: Can this be added to the candidates list?
    #local_candidates*: list    ##  TODO: Hash list instead?
    ice_username_fragment*: array[ICE_USERNAME_FRAGMENT_LENGTH + 1, char]
    ice_password*: array[ICE_PASSWORD_LENGTH + 1, char]
    #ice*: ptr trice
    #ice_config*: trice_conf
    #dns_client*: ptr dnsc


##
##  ICE transport.
##  TODO: private
##

type
  rawrtc_ice_transport* {.bycopy.} = object
    state*: rawrtc_ice_transport_state
    gatherer*: ptr rawrtc_ice_gatherer ##  referenced
    state_change_handler*: ptr rawrtc_ice_transport_state_change_handler ##  nullable
    candidate_pair_change_handler*: ptr rawrtc_ice_transport_candidate_pair_change_handler ##  nullable
    arg*: pointer              ##  nullable
    remote_parameters*: ptr rawrtc_ice_parameters ##  referenced
    dtls_transport*: ptr rawrtc_dtls_transport ##  referenced, nullable


##
##  DTLS fingerprint.
##  TODO: private
##

type
  rawrtc_dtls_fingerprint* {.bycopy.} = object
    #le*: le
    algorithm*: rawrtc_certificate_sign_algorithm
    value*: cstring          ##  copied


##
##  DTLS fingerprints.
##  Note: Inherits `struct rawrtc_array_container`.
##

type
  rawrtc_dtls_fingerprints* {.bycopy.} = object
    n_fingerprints*: csize
    fingerprints*: UncheckedArray[ptr rawrtc_dtls_fingerprint]

##
##  DTLS parameters.
##  TODO: private
##

type
  rawrtc_dtls_parameters* {.bycopy.} = object
    role*: rawrtc_dtls_role
    fingerprints*: ptr rawrtc_dtls_fingerprints


##
##  DTLS transport.
##  TODO: private
##

#[type
  rawrtc_dtls_transport* {.bycopy.} = object
    state*: rawrtc_dtls_transport_state
    ice_transport*: ptr rawrtc_ice_transport ##  referenced
    certificates*: list        ##  deep-copied
    state_change_handler*: ptr rawrtc_dtls_transport_state_change_handler ##  nullable
    error_handler*: ptr rawrtc_dtls_transport_error_handler ##  nullable
    arg*: pointer              ##  nullable
    remote_parameters*: ptr rawrtc_dtls_parameters ##  referenced
    role*: rawrtc_dtls_role
    connection_established*: bool
    buffered_messages_in*: list
    buffered_messages_out*: list
    fingerprints*: list
    context*: ptr tls
    socket*: ptr dtls_sock
    connection*: ptr tls_conn
    receive_handler*: ptr rawrtc_dtls_transport_receive_handler
    receive_handler_arg*: pointer]#


when defined(SCTP_REDIRECT_TRANSPORT):
  ##
  ##  Redirect transport.
  ##
  type
    rawrtc_sctp_redirect_transport* {.bycopy.} = object
      state*: rawrtc_sctp_redirect_transport_state
      dtls_transport*: ptr rawrtc_dtls_transport ##  referenced
      local_port*: uint16
      remote_port*: uint16
      redirect_address*: sa
      buffer*: ptr mbuf
      socket*: cint

##
##  Generic data transport.
##  TODO: private
##

#[type
  rawrtc_data_transport* {.bycopy.} = object
    `type`*: rawrtc_data_transport_type ##  TODO: Can this be removed?
    transport*: pointer
    channel_create*: ptr rawrtc_data_transport_channel_create_handler
    channel_close*: ptr rawrtc_data_transport_channel_close_handler
    channel_send*: ptr rawrtc_data_transport_channel_send_handler]#


##
##  Data channel parameters.
##  TODO: private
##

#[type
  rawrtc_data_channel_parameters* {.bycopy.} = object
    label*: cstring            ##  copied
    channel_type*: rawrtc_data_channel_type
    reliability_parameter*: uint32 ##  contains either max_packet_lifetime or max_retransmit
    protocol*: cstring         ##  copied
    negotiated*: bool
    id*: uint16]#


##
##  Data channel options.
##  TODO: private
##

type
  rawrtc_data_channel_options* {.bycopy.} = object
    deliver_partially*: bool


##
##  SCTP capabilities.
##  TODO: private
##

type
  rawrtc_sctp_capabilities* {.bycopy.} = object
    max_message_size*: uint64


##
##  SCTP transport.
##  TODO: private
##

#[type
  rawrtc_sctp_transport* {.bycopy.} = object
    state*: rawrtc_sctp_transport_state
    port*: uint16
    remote_maximum_message_size*: uint64
    dtls_transport*: ptr rawrtc_dtls_transport ##  referenced
    data_channel_handler*: ptr rawrtc_data_channel_handler ##  nullable
    state_change_handler*: ptr rawrtc_sctp_transport_state_change_handler ##  nullable
    arg*: pointer              ##  nullable
    buffered_messages_outgoing*: list
    buffer_dcep_inbound*: ptr mbuf
    info_dcep_inbound*: sctp_rcvinfo
    channels*: ptr ptr rawrtc_data_channel
    n_channels*: uint16
    current_channel_sid*: uint16
    trace_handle*: ptr FILE
    socket*: ptr socket
    flags*: uint8]#


##
##  SCTP data channel context.
##  TODO: private
##

type
  rawrtc_sctp_data_channel_context* {.bycopy.} = object
    sid*: uint16
    flags*: uint8
    buffer_inbound*: ptr mbuf
    #info_inbound*: sctp_rcvinfo


##
##  Data channel.
##  TODO: private
##

#[type
  rawrtc_data_channel* {.bycopy.} = object
    flags*: uint_fast8_t
    state*: rawrtc_data_channel_state
    transport*: ptr rawrtc_data_transport ##  referenced
    transport_arg*: pointer    ##  referenced
    parameters*: ptr rawrtc_data_channel_parameters ##  referenced
    options*: ptr rawrtc_data_channel_options ##  nullable, referenced
    open_handler*: ptr rawrtc_data_channel_open_handler ##  nullable
    buffered_amount_low_handler*: ptr rawrtc_data_channel_buffered_amount_low_handler ##  nullable
    error_handler*: ptr rawrtc_data_channel_error_handler ##  nullable
    close_handler*: ptr rawrtc_data_channel_close_handler ##  nullable
    message_handler*: ptr rawrtc_data_channel_message_handler ##  nullable
    arg*: pointer              ##  nullable]#


##
##  Peer connection configuration.
##

type
  rawrtc_peer_connection_configuration* {.bycopy.} = object
    gather_policy*: rawrtc_ice_gather_policy
    #ice_servers*: list
    #certificates*: list
    sctp_sdp_05*: bool


##
##  Peer connection ICE candidate.
##  TODO: private
##

#[type
  rawrtc_peer_connection_ice_candidate* {.bycopy.} = object
    le*: le
    candidate*: ptr rawrtc_ice_candidate
    mid*: cstring
    media_line_index*: int16_t
    username_fragment*: cstring]#


##
##  Peer connection context.
##  TODO: private
##

type
  rawrtc_peer_connection_context* {.bycopy.} = object
    gather_options*: ptr rawrtc_ice_gather_options
    ice_gatherer*: ptr rawrtc_ice_gatherer
    ice_transport*: ptr rawrtc_ice_transport
    #certificates*: list
    dtls_id*: array[DTLS_ID_LENGTH + 1, char]
    dtls_transport*: ptr rawrtc_dtls_transport
    data_transport*: ptr rawrtc_data_transport

##
##  Peer connection.
##  TODO: private
##

type
  rawrtc_peer_connection* {.bycopy.} = object
    connection_state*: rawrtc_peer_connection_state
    signaling_state*: rawrtc_signaling_state
    configuration*: ptr rawrtc_peer_connection_configuration ##  referenced
    negotiation_needed_handler*: ptr rawrtc_negotiation_needed_handler ##  nullable
    local_candidate_handler*: ptr rawrtc_peer_connection_local_candidate_handler ##  nullable
    local_candidate_error_handler*: ptr rawrtc_peer_connection_local_candidate_error_handler ##  nullable
    signaling_state_change_handler*: ptr rawrtc_signaling_state_change_handler ##  nullable
    ice_connection_state_change_handler*: ptr rawrtc_ice_transport_state_change_handler ##  nullable
    ice_gathering_state_change_handler*: ptr rawrtc_ice_gatherer_state_change_handler ##  nullable
    connection_state_change_handler*: ptr rawrtc_peer_connection_state_change_handler ##  nullable
    data_channel_handler*: ptr rawrtc_data_channel_handler ##  nullable
    data_transport_type*: rawrtc_data_transport_type
    #local_description*: ptr rawrtc_peer_connection_description ##  referenced
    #remote_description*: ptr rawrtc_peer_connection_description ##  referenced
    context*: rawrtc_peer_connection_context
    arg*: pointer              ##  nullable

##
##  Peer connection description.
##  TODO: private
##

type
  rawrtc_peer_connection_description* {.bycopy.} = object
    connection*: ptr rawrtc_peer_connection
    `type`*: rawrtc_sdp_type
    trickle_ice*: bool
    bundled_mids*: cstring
    remote_media_line*: cstring
    media_line_index*: uint8
    mid*: cstring
    sctp_sdp_05*: bool
    end_of_candidates*: bool
    #ice_candidates*: list
    ice_parameters*: ptr rawrtc_ice_parameters
    dtls_parameters*: ptr rawrtc_dtls_parameters
    sctp_capabilities*: ptr rawrtc_sctp_capabilities
    sctp_port*: uint16
    sdp*: ptr mbuf


##
##  Layers.
##  TODO: private
##

const
  RAWRTC_LAYER_SCTP* = 20
  RAWRTC_LAYER_DTLS_SRTP_STUN* = 10 ##  TODO: Pretty sure we are able to detect STUN earlier
  RAWRTC_LAYER_ICE* = 0
  RAWRTC_LAYER_STUN* = -10
  RAWRTC_LAYER_TURN* = -10

##
##  Array container.
##  TODO: private
##

type
  rawrtc_array_container* {.bycopy.} = object
    n_items*: csize
    items*: ptr pointer


##
##  Certificates.
##  Note: Inherits `struct rawrtc_array_container`.
##

type
  rawrtc_certificates* {.bycopy.} = object
    n_certificates*: csize
    certificates*: ptr ptr rawrtc_certificate


##
##  ICE servers.
##  Note: Inherits `struct rawrtc_array_container`.
##

type
  rawrtc_ice_servers* {.bycopy.} = object
    n_servers*: csize
    servers*: ptr ptr rawrtc_ice_server


##
##  ICE candidates.
##  Note: Inherits `struct rawrtc_array_container`.
##

type
  rawrtc_ice_candidates* {.bycopy.} = object
    n_candidates*: csize
    candidates*: UncheckedArray[ptr rawrtc_ice_candidate]


##
##  Initialise rawrtc. Must be called before making a call to any other
##  function.
##

proc rawrtc_init*(): rawrtc_code {.cdecl, importc: "rawrtc_init", dynlib: librawrtc.}
##
##  Close rawrtc and free up all resources.
##

proc rawrtc_close*(): rawrtc_code {.cdecl, importc: "rawrtc_close", dynlib: librawrtc.}
##
##  Create certificate options.
##
##  All arguments but `key_type` are optional. Sane and safe default
##  values will be applied, don't worry!
##
##  If `common_name` is `NULL` the default common name will be applied.
##  If `valid_until` is `0` the default certificate lifetime will be
##  applied.
##  If the key type is `ECC` and `named_curve` is `NULL`, the default
##  named curve will be used.
##  If the key type is `RSA` and `modulus_length` is `0`, the default
##  amount of bits will be used. The same applies to the
##  `sign_algorithm` if it has been set to `NONE`.
##

proc rawrtc_certificate_options_create*(optionsp: ptr ptr rawrtc_certificate_options;
                                       key_type: rawrtc_certificate_key_type;
                                       common_name: cstring;
                                       valid_until: uint32; sign_algorithm: rawrtc_certificate_sign_algorithm;
                                       named_curve: cstring; modulus_length: uint32): rawrtc_code {.
    cdecl, importc: "rawrtc_certificate_options_create", dynlib: librawrtc.}
  ##  de-referenced
  ##  nullable, copied
  ##  nullable, copied, ignored for RSA
  ##  ignored for ECC
##
##  Create and generate a self-signed certificate.
##
##  Sane and safe default options will be applied if `options` is
##  `NULL`.
##

proc rawrtc_certificate_generate*(certificatep: ptr ptr rawrtc_certificate; options: ptr rawrtc_certificate_options): rawrtc_code {.
    cdecl, importc: "rawrtc_certificate_generate", dynlib: librawrtc.}
  ##  nullable
##
##  TODO http://draft.ortc.org/#dom-rtccertificate
##  rawrtc_certificate_from_bytes
##  rawrtc_certificate_get_expires
##  rawrtc_certificate_get_fingerprint
##  rawrtc_certificate_get_algorithm
##
##
##  Create an ICE candidate.
##

proc rawrtc_ice_candidate_create*(candidatep: ptr ptr rawrtc_ice_candidate;
                                 foundation: cstring; priority: uint32;
                                 ip: cstring; protocol: rawrtc_ice_protocol;
                                 port: uint16;
                                 `type`: rawrtc_ice_candidate_type;
                                 tcp_type: rawrtc_ice_tcp_candidate_type;
                                 related_address: cstring; related_port: uint16): rawrtc_code {.
    cdecl, importc: "rawrtc_ice_candidate_create", dynlib: librawrtc.}
  ##  de-referenced
  ##  copied
  ##  copied
  ##  copied, nullable
##
##  Get the ICE candidate's foundation.
##  `*foundationp` will be set to a copy of the IP address that must be
##  unreferenced.
##

proc rawrtc_ice_candidate_get_foundation*(foundationp: ptr cstring;
    candidate: ptr rawrtc_ice_candidate): rawrtc_code {.cdecl,
    importc: "rawrtc_ice_candidate_get_foundation", dynlib: librawrtc.}
  ##  de-referenced
##
##  Get the ICE candidate's priority.
##

proc rawrtc_ice_candidate_get_priority*(priorityp: ptr uint32;
                                       candidate: ptr rawrtc_ice_candidate): rawrtc_code {.
    cdecl, importc: "rawrtc_ice_candidate_get_priority", dynlib: librawrtc.}
  ##  de-referenced
##
##  Get the ICE candidate's IP address.
##  `*ipp` will be set to a copy of the IP address that must be
##  unreferenced.
##

proc rawrtc_ice_candidate_get_ip*(ipp: ptr cstring;
                                 candidate: ptr rawrtc_ice_candidate): rawrtc_code {.
    cdecl, importc: "rawrtc_ice_candidate_get_ip", dynlib: librawrtc.}
  ##  de-referenced
##
##  Get the ICE candidate's protocol.
##

proc rawrtc_ice_candidate_get_protocol*(protocolp: ptr rawrtc_ice_protocol;
                                       candidate: ptr rawrtc_ice_candidate): rawrtc_code {.
    cdecl, importc: "rawrtc_ice_candidate_get_protocol", dynlib: librawrtc.}
  ##  de-referenced
##
##  Get the ICE candidate's port.
##

proc rawrtc_ice_candidate_get_port*(portp: ptr uint16;
                                   candidate: ptr rawrtc_ice_candidate): rawrtc_code {.
    cdecl, importc: "rawrtc_ice_candidate_get_port", dynlib: librawrtc.}
  ##  de-referenced
##
##  Get the ICE candidate's type.
##

proc rawrtc_ice_candidate_get_type*(typep: ptr rawrtc_ice_candidate_type;
                                   candidate: ptr rawrtc_ice_candidate): rawrtc_code {.
    cdecl, importc: "rawrtc_ice_candidate_get_type", dynlib: librawrtc.}
  ##  de-referenced
##
##  Get the ICE candidate's TCP type.
##  `*typep` will be set to `NULL` in case the protocol is not TCP.
##

proc rawrtc_ice_candidate_get_tcp_type*(typep: ptr rawrtc_ice_tcp_candidate_type;
                                       candidate: ptr rawrtc_ice_candidate): rawrtc_code {.
    cdecl, importc: "rawrtc_ice_candidate_get_tcp_type", dynlib: librawrtc.}
  ##  de-referenced
##
##  Get the ICE candidate's related IP address.
##  `*related_address` will be set to a copy of the related address that
##  must be unreferenced or `NULL` in case no related address exists.
##

proc rawrtc_ice_candidate_get_related_address*(related_addressp: ptr cstring;
    candidate: ptr rawrtc_ice_candidate): rawrtc_code {.cdecl,
    importc: "rawrtc_ice_candidate_get_related_address", dynlib: librawrtc.}
  ##  de-referenced
##
##  Get the ICE candidate's related IP address' port.
##  `*related_portp` will be set to a copy of the related address'
##  port or `0` in case no related address exists.
##

proc rawrtc_ice_candidate_get_related_port*(related_portp: ptr uint16;
    candidate: ptr rawrtc_ice_candidate): rawrtc_code {.cdecl,
    importc: "rawrtc_ice_candidate_get_related_port", dynlib: librawrtc.}
  ##  de-referenced
##
##  Create a new ICE parameters instance.
##

proc rawrtc_ice_parameters_create*(parametersp: ptr ptr rawrtc_ice_parameters;
                                  username_fragment: cstring; password: cstring;
                                  ice_lite: bool): rawrtc_code {.cdecl,
    importc: "rawrtc_ice_parameters_create", dynlib: librawrtc.}
  ##  de-referenced
  ##  copied
  ##  copied
##
##  Get the ICE parameter's username fragment value.
##  `*username_fragmentp` must be unreferenced.
##

proc rawrtc_ice_parameters_get_username_fragment*(
    username_fragmentp: cstringArray; parameters: ptr rawrtc_ice_parameters): rawrtc_code {.
    cdecl, importc: "rawrtc_ice_parameters_get_username_fragment",
    dynlib: librawrtc.}
  ##  de-referenced
##
##  Get the ICE parameter's password value.
##  `*passwordp` must be unreferenced.
##

proc rawrtc_ice_parameters_get_password*(passwordp: cstringArray;
                                        parameters: ptr rawrtc_ice_parameters): rawrtc_code {.
    cdecl, importc: "rawrtc_ice_parameters_get_password", dynlib: librawrtc.}
  ##  de-referenced
##
##  Get the ICE parameter's ICE lite value.
##

proc rawrtc_ice_parameters_get_ice_lite*(ice_litep: ptr bool;
                                        parameters: ptr rawrtc_ice_parameters): rawrtc_code {.
    cdecl, importc: "rawrtc_ice_parameters_get_ice_lite", dynlib: librawrtc.}
  ##  de-referenced
##
##  Create a new ICE gather options instance.
##

proc rawrtc_ice_gather_options_create*(optionsp: ptr ptr rawrtc_ice_gather_options;
                                      gather_policy: rawrtc_ice_gather_policy): rawrtc_code {.
    cdecl, importc: "rawrtc_ice_gather_options_create", dynlib: librawrtc.}
  ##  de-referenced
##
##  TODO
##  rawrtc_ice_server_list_*
##
##
##  Add an ICE server to the gather options.
##

proc rawrtc_ice_gather_options_add_server*(
    options: ptr rawrtc_ice_gather_options; urls: cstringArray; n_urls: csize;
    username: cstring; credential: cstring;
    credential_type: rawrtc_ice_credential_type): rawrtc_code {.cdecl,
    importc: "rawrtc_ice_gather_options_add_server", dynlib: librawrtc.}
  ##  copied
  ##  nullable, copied
  ##  nullable, copied
##
##  TODO (from RTCIceServer interface)
##  rawrtc_ice_server_set_username
##  rawrtc_ice_server_set_credential
##  rawrtc_ice_server_set_credential_type
##
##
##  Get the corresponding name for an ICE gatherer state.
##

proc rawrtc_ice_gatherer_state_to_name*(state: rawrtc_ice_gatherer_state): cstring {.
    cdecl, importc: "rawrtc_ice_gatherer_state_to_name", dynlib: librawrtc.}
##
##  Create a new ICE gatherer.
##

proc rawrtc_ice_gatherer_create*(gathererp: ptr ptr rawrtc_ice_gatherer;
                                options: ptr rawrtc_ice_gather_options;
    state_change_handler: rawrtc_ice_gatherer_state_change_handler;
    error_handler:  rawrtc_ice_gatherer_error_handler; local_candidate_handler: rawrtc_ice_gatherer_local_candidate_handler; arg: pointer): rawrtc_code {.
    cdecl, importc: "rawrtc_ice_gatherer_create", dynlib: librawrtc.}
  ##  de-referenced
  ##  referenced
  ##  nullable
  ##  nullable
  ##  nullable
  ##  nullable
##
##  Close the ICE gatherer.
##

proc rawrtc_ice_gatherer_close*(gatherer: ptr rawrtc_ice_gatherer): rawrtc_code {.
    cdecl, importc: "rawrtc_ice_gatherer_close", dynlib: librawrtc.}
##
##  Start gathering using an ICE gatherer.
##

proc rawrtc_ice_gatherer_gather*(gatherer: ptr rawrtc_ice_gatherer; options: ptr rawrtc_ice_gather_options): rawrtc_code {.
    cdecl, importc: "rawrtc_ice_gatherer_gather", dynlib: librawrtc.}
  ##  referenced, nullable
##
##  TODO (from RTCIceGatherer interface)
##  rawrtc_ice_gatherer_get_component
##
##
##  Get the current state of an ICE gatherer.
##

proc rawrtc_ice_gatherer_get_state*(statep: ptr rawrtc_ice_gatherer_state;
                                   gatherer: ptr rawrtc_ice_gatherer): rawrtc_code {.
    cdecl, importc: "rawrtc_ice_gatherer_get_state", dynlib: librawrtc.}
  ##  de-referenced
##
##  Get local ICE parameters of an ICE gatherer.
##

proc rawrtc_ice_gatherer_get_local_parameters*(
    parametersp: ptr ptr rawrtc_ice_parameters; gatherer: ptr rawrtc_ice_gatherer): rawrtc_code {.
    cdecl, importc: "rawrtc_ice_gatherer_get_local_parameters", dynlib: librawrtc.}
  ##  de-referenced
##
##  Get local ICE candidates of an ICE gatherer.
##

proc rawrtc_ice_gatherer_get_local_candidates*(
    candidatesp: ptr ptr rawrtc_ice_candidates; gatherer: ptr rawrtc_ice_gatherer): rawrtc_code {.
    cdecl, importc: "rawrtc_ice_gatherer_get_local_candidates", dynlib: librawrtc.}
  ##  de-referenced
##
##  TODO (from RTCIceGatherer interface)
##  rawrtc_ice_gatherer_create_associated_gatherer (unsupported)
##  rawrtc_ice_gatherer_set_state_change_handler
##  rawrtc_ice_gatherer_set_error_handler
##  rawrtc_ice_gatherer_set_local_candidate_handler
##
##
##  Get the corresponding name for an ICE transport state.
##

proc rawrtc_ice_transport_state_to_name*(state: rawrtc_ice_transport_state): cstring {.
    cdecl, importc: "rawrtc_ice_transport_state_to_name", dynlib: librawrtc.}
##
##  Create a new ICE transport.
##

proc rawrtc_ice_transport_create*(transportp: ptr ptr rawrtc_ice_transport;
                                 gatherer: ptr rawrtc_ice_gatherer;
    state_change_handler: rawrtc_ice_transport_state_change_handler;
    candidate_pair_change_handler: rawrtc_ice_transport_candidate_pair_change_handler; arg: pointer): rawrtc_code {.
    cdecl, importc: "rawrtc_ice_transport_create", dynlib: librawrtc.}
  ##  de-referenced
  ##  referenced, nullable
  ##  nullable
  ##  nullable
  ##  nullable
##
##  Start the ICE transport.
##

proc rawrtc_ice_transport_start*(transport: ptr rawrtc_ice_transport;
                                gatherer: ptr rawrtc_ice_gatherer;
                                remote_parameters: ptr rawrtc_ice_parameters;
                                role: rawrtc_ice_role): rawrtc_code {.cdecl,
    importc: "rawrtc_ice_transport_start", dynlib: librawrtc.}
  ##  referenced
  ##  referenced
##
##  Stop and close the ICE transport.
##

proc rawrtc_ice_transport_stop*(transport: ptr rawrtc_ice_transport): rawrtc_code {.
    cdecl, importc: "rawrtc_ice_transport_stop", dynlib: librawrtc.}
##
##  TODO (from RTCIceTransport interface)
##  rawrtc_ice_transport_get_ice_gatherer
##
##
##  Get the current ICE role of the ICE transport.
##

proc rawrtc_ice_transport_get_role*(rolep: ptr rawrtc_ice_role;
                                   transport: ptr rawrtc_ice_transport): rawrtc_code {.
    cdecl, importc: "rawrtc_ice_transport_get_role", dynlib: librawrtc.}
  ##  de-referenced
##
##  TODO
##  rawrtc_ice_transport_get_component
##
##
##  Get the current state of the ICE transport.
##

proc rawrtc_ice_transport_get_state*(statep: ptr rawrtc_ice_transport_state;
                                    transport: ptr rawrtc_ice_transport): rawrtc_code {.
    cdecl, importc: "rawrtc_ice_transport_get_state", dynlib: librawrtc.}
  ##  de-referenced
##
##  rawrtc_ice_transport_get_remote_candidates
##  rawrtc_ice_transport_get_selected_candidate_pair
##  rawrtc_ice_transport_get_remote_parameters
##  rawrtc_ice_transport_create_associated_transport (unsupported)
##
##
##  Add a remote candidate ot the ICE transport.
##  Note: 'candidate' must be NULL to inform the transport that the
##  remote site finished gathering.
##

proc rawrtc_ice_transport_add_remote_candidate*(
    transport: ptr rawrtc_ice_transport; candidate: ptr rawrtc_ice_candidate): rawrtc_code {.
    cdecl, importc: "rawrtc_ice_transport_add_remote_candidate", dynlib: librawrtc.}
  ##  nullable
##
##  Set the remote candidates on the ICE transport overwriting all
##  existing remote candidates.
##

proc rawrtc_ice_transport_set_remote_candidates*(
    transport: ptr rawrtc_ice_transport; candidates: ptr ptr rawrtc_ice_candidate;
    n_candidates: csize): rawrtc_code {.cdecl, importc: "rawrtc_ice_transport_set_remote_candidates",
                                     dynlib: librawrtc.}
  ##  referenced (each item)
##  TODO (from RTCIceTransport interface)
##  rawrtc_ice_transport_set_state_change_handler
##  rawrtc_ice_transport_set_candidate_pair_change_handler
##
##
##  Create a new DTLS fingerprint instance.
##

proc rawrtc_dtls_fingerprint_create*(fingerprintp: ptr ptr rawrtc_dtls_fingerprint;
    algorithm: rawrtc_certificate_sign_algorithm; value: cstring): rawrtc_code {.
    cdecl, importc: "rawrtc_dtls_fingerprint_create", dynlib: librawrtc.}
  ##  de-referenced
  ##  copied
##
##  TODO
##  rawrtc_dtls_fingerprint_get_algorithm
##  rawrtc_dtls_fingerprint_get_value
##
##
##  Create a new DTLS parameters instance.
##

proc rawrtc_dtls_parameters_create*(parametersp: ptr ptr rawrtc_dtls_parameters;
                                   role: rawrtc_dtls_role; fingerprints: ptr ptr rawrtc_dtls_fingerprint;
                                   n_fingerprints: csize): rawrtc_code {.cdecl,
    importc: "rawrtc_dtls_parameters_create", dynlib: librawrtc.}
  ##  de-referenced
  ##  referenced (each item)
##
##  Get the DTLS parameter's role value.
##

proc rawrtc_dtls_parameters_get_role*(rolep: ptr rawrtc_dtls_role;
                                     parameters: ptr rawrtc_dtls_parameters): rawrtc_code {.
    cdecl, importc: "rawrtc_dtls_parameters_get_role", dynlib: librawrtc.}
  ##  de-referenced
##
##  Get the DTLS parameter's fingerprint array.
##  `*fingerprintsp` must be unreferenced.
##

proc rawrtc_dtls_parameters_get_fingerprints*(
    fingerprintsp: ptr ptr rawrtc_dtls_fingerprints;
    parameters: ptr rawrtc_dtls_parameters): rawrtc_code {.cdecl,
    importc: "rawrtc_dtls_parameters_get_fingerprints", dynlib: librawrtc.}
  ##  de-referenced
##
##  Get the DTLS certificate fingerprint's sign algorithm.
##

proc rawrtc_dtls_parameters_fingerprint_get_sign_algorithm*(
    sign_algorithmp: ptr rawrtc_certificate_sign_algorithm;
    fingerprint: ptr rawrtc_dtls_fingerprint): rawrtc_code {.cdecl,
    importc: "rawrtc_dtls_parameters_fingerprint_get_sign_algorithm",
    dynlib: librawrtc.}
  ##  de-referenced
##
##  Get the DTLS certificate's fingerprint value.
##  `*valuep` must be unreferenced.
##

proc rawrtc_dtls_parameters_fingerprint_get_value*(valuep: cstringArray;
    fingerprint: ptr rawrtc_dtls_fingerprint): rawrtc_code {.cdecl,
    importc: "rawrtc_dtls_parameters_fingerprint_get_value", dynlib: librawrtc.}
  ##  de-referenced
##
##  Get the corresponding name for an ICE transport state.
##

proc rawrtc_dtls_transport_state_to_name*(state: rawrtc_dtls_transport_state): cstring {.
    cdecl, importc: "rawrtc_dtls_transport_state_to_name", dynlib: librawrtc.}
##
##  Create a new DTLS transport.
##

proc rawrtc_dtls_transport_create*(transportp: ptr ptr rawrtc_dtls_transport;
                                  ice_transport: ptr rawrtc_ice_transport;
                                  certificates: openarray[ptr rawrtc_certificate];
                                  n_certificates: csize; state_change_handler: rawrtc_dtls_transport_state_change_handler;
    error_handler: rawrtc_dtls_transport_error_handler; arg: pointer): rawrtc_code {.
    cdecl, importc: "rawrtc_dtls_transport_create", dynlib: librawrtc.}
  ##  de-referenced
  ##  referenced
  ##  copied (each item)
  ##  nullable
  ##  nullable
  ##  nullable
##
##  Start the DTLS transport.
##

proc rawrtc_dtls_transport_start*(transport: ptr rawrtc_dtls_transport; remote_parameters: ptr rawrtc_dtls_parameters): rawrtc_code {.
    cdecl, importc: "rawrtc_dtls_transport_start", dynlib: librawrtc.}
  ##  copied
##
##  Stop and close the DTLS transport.
##

proc rawrtc_dtls_transport_stop*(transport: ptr rawrtc_dtls_transport): rawrtc_code {.
    cdecl, importc: "rawrtc_dtls_transport_stop", dynlib: librawrtc.}
##
##  TODO (from RTCIceTransport interface)
##  rawrtc_certificate_list_*
##  rawrtc_dtls_transport_get_certificates
##  rawrtc_dtls_transport_get_transport
##
##
##  Get the current state of the DTLS transport.
##

proc rawrtc_dtls_transport_get_state*(statep: ptr rawrtc_dtls_transport_state;
                                     transport: ptr rawrtc_dtls_transport): rawrtc_code {.
    cdecl, importc: "rawrtc_dtls_transport_get_state", dynlib: librawrtc.}
  ##  de-referenced
##
##  Get local DTLS parameters of a transport.
##

proc rawrtc_dtls_transport_get_local_parameters*(
    parametersp: ptr ptr rawrtc_dtls_parameters;
    transport: ptr rawrtc_dtls_transport): rawrtc_code {.cdecl,
    importc: "rawrtc_dtls_transport_get_local_parameters", dynlib: librawrtc.}
  ##  de-referenced
##
##  TODO (from RTCIceTransport interface)
##  rawrtc_dtls_transport_get_remote_parameters
##  rawrtc_dtls_transport_get_remote_certificates
##  rawrtc_dtls_transport_set_state_change_handler
##  rawrtc_dtls_transport_set_error_handler
##

when defined(SCTP_REDIRECT_TRANSPORT):
  ##
  ##  Create an SCTP redirect transport.
  ##
  proc rawrtc_sctp_redirect_transport_create*(
      transportp: ptr ptr rawrtc_sctp_redirect_transport;
      dtls_transport: ptr rawrtc_dtls_transport; port: uint16;
      redirect_ip: cstring; redirect_port: uint16): rawrtc_code {.cdecl,
      importc: "rawrtc_sctp_redirect_transport_create", dynlib: librawrtc.}
    ##  de-referenced
    ##  referenced
    ##  zeroable
    ##  copied
  ##
  ##  Start an SCTP redirect transport.
  ##
  proc rawrtc_sctp_redirect_transport_start*(
      transport: ptr rawrtc_sctp_redirect_transport;
      remote_capabilities: ptr rawrtc_sctp_capabilities; remote_port: uint16): rawrtc_code {.
      cdecl, importc: "rawrtc_sctp_redirect_transport_start", dynlib: librawrtc.}
    ##  copied
    ##  zeroable
  ##
  ##  Stop and close the SCTP redirect transport.
  ##
  proc rawrtc_sctp_redirect_transport_stop*(
      transport: ptr rawrtc_sctp_redirect_transport): rawrtc_code {.cdecl,
      importc: "rawrtc_sctp_redirect_transport_stop", dynlib: librawrtc.}
  ##
  ##  Get the redirected local SCTP port of the SCTP redirect transport.
  ##
  proc rawrtc_sctp_redirect_transport_get_port*(portp: ptr uint16;
      transport: ptr rawrtc_sctp_redirect_transport): rawrtc_code {.cdecl,
      importc: "rawrtc_sctp_redirect_transport_get_port", dynlib: librawrtc.}
    ##  de-referenced
##
##  Create a new SCTP transport capabilities instance.
##

proc rawrtc_sctp_capabilities_create*(capabilitiesp: ptr ptr rawrtc_sctp_capabilities;
                                     max_message_size: uint64): rawrtc_code {.
    cdecl, importc: "rawrtc_sctp_capabilities_create", dynlib: librawrtc.}
  ##  de-referenced
##
##  Get the SCTP parameter's maximum message size value.
##

proc rawrtc_sctp_capabilities_get_max_message_size*(
    max_message_sizep: ptr uint64; capabilities: ptr rawrtc_sctp_capabilities): rawrtc_code {.
    cdecl, importc: "rawrtc_sctp_capabilities_get_max_message_size",
    dynlib: librawrtc.}
  ##  de-referenced
##
##  Get the corresponding name for an SCTP transport state.
##

proc rawrtc_sctp_transport_state_to_name*(state: rawrtc_sctp_transport_state): cstring {.
    cdecl, importc: "rawrtc_sctp_transport_state_to_name", dynlib: librawrtc.}
##
##  Create an SCTP transport.
##

proc rawrtc_sctp_transport_create*(transportp: ptr ptr rawrtc_sctp_transport;
                                  dtls_transport: ptr rawrtc_dtls_transport;
                                  port: uint16; data_channel_handler: rawrtc_data_channel_handler;
    state_change_handler: rawrtc_sctp_transport_state_change_handler; arg: pointer): rawrtc_code {.
    cdecl, importc: "rawrtc_sctp_transport_create", dynlib: librawrtc.}
  ##  de-referenced
  ##  referenced
  ##  zeroable
  ##  nullable
  ##  nullable
  ##  nullable
##
##  Get the SCTP data transport instance.
##

proc rawrtc_sctp_transport_get_data_transport*(
    transportp: ptr ptr rawrtc_data_transport; sctp_transport: ptr rawrtc_sctp_transport): rawrtc_code {.
    cdecl, importc: "rawrtc_sctp_transport_get_data_transport", dynlib: librawrtc.}
  ##  de-referenced
  ##  referenced
##
##  Start the SCTP transport.
##

proc rawrtc_sctp_transport_start*(transport: ptr rawrtc_sctp_transport;
    remote_capabilities: ptr rawrtc_sctp_capabilities; remote_port: uint16): rawrtc_code {.
    cdecl, importc: "rawrtc_sctp_transport_start", dynlib: librawrtc.}
  ##  copied
  ##  zeroable
##
##  Stop and close the SCTP transport.
##

proc rawrtc_sctp_transport_stop*(transport: ptr rawrtc_sctp_transport): rawrtc_code {.
    cdecl, importc: "rawrtc_sctp_transport_stop", dynlib: librawrtc.}
##
##  TODO (from RTCSctpTransport interface)
##  rawrtc_sctp_transport_get_transport
##  rawrtc_sctp_transport_get_state
##
##
##  Get the local port of the SCTP transport.
##

proc rawrtc_sctp_transport_get_port*(portp: ptr uint16;
                                    transport: ptr rawrtc_sctp_transport): rawrtc_code {.
    cdecl, importc: "rawrtc_sctp_transport_get_port", dynlib: librawrtc.}
  ##  de-referenced
##
##  Get the local SCTP transport capabilities (static).
##

proc rawrtc_sctp_transport_get_capabilities*(capabilitiesp: ptr ptr rawrtc_sctp_capabilities): rawrtc_code {.
    cdecl, importc: "rawrtc_sctp_transport_get_capabilities", dynlib: librawrtc.}
  ##  de-referenced
##
##  TODO (from RTCSctpTransport interface)
##  rawrtc_sctp_transport_set_data_channel_handler
##
##
##  Create data channel parameters.
##
##  For `RAWRTC_DATA_CHANNEL_TYPE_RELIABLE_*`, the reliability parameter
##  is being ignored.
##
##  When using `RAWRTC_DATA_CHANNEL_TYPE_*_RETRANSMIT`, the reliability
##  parameter specifies the number of times a retransmission occurs if
##  not acknowledged before the message is being discarded.
##
##  When using `RAWRTC_DATA_CHANNEL_TYPE_*_TIMED`, the reliability
##  parameter specifies the time window in milliseconds during which
##  (re-)transmissions may occur before the message is being discarded.
##
##  In case `negotiated` is set to `false`, the `id` is being ignored.
##

proc rawrtc_data_channel_parameters_create*(
    parametersp: ptr ptr rawrtc_data_channel_parameters; label: cstring;
    channel_type: rawrtc_data_channel_type; reliability_parameter: uint32;
    protocol: cstring; negotiated: bool; id: uint16): rawrtc_code {.cdecl,
    importc: "rawrtc_data_channel_parameters_create", dynlib: librawrtc.}
  ##  de-referenced
  ##  copied, nullable
  ##  copied
##
##  Get the label from the data channel parameters.
##  Return `RAWRTC_CODE_NO_VALUE` in case no label has been set.
##  Otherwise, `RAWRTC_CODE_SUCCESS` will be returned and `*parameters*
##  must be unreferenced.
##

proc rawrtc_data_channel_parameters_get_label*(labelp: cstringArray;
    parameters: ptr rawrtc_data_channel_parameters): rawrtc_code {.cdecl,
    importc: "rawrtc_data_channel_parameters_get_label", dynlib: librawrtc.}
  ##  de-referenced
##
##  TODO
##  rawrtc_data_channel_parameters_get_channel_type
##  rawrtc_data_channel_parameters_get_reliability_parameter
##
##
##  Get the protocol from the data channel parameters.
##  Return `RAWRTC_CODE_NO_VALUE` in case no protocol has been set.
##  Otherwise, `RAWRTC_CODE_SUCCESS` will be returned and `*protocolp*
##  must be unreferenced.
##

proc rawrtc_data_channel_parameters_get_protocol*(protocolp: cstringArray;
    parameters: ptr rawrtc_data_channel_parameters): rawrtc_code {.cdecl,
    importc: "rawrtc_data_channel_parameters_get_protocol", dynlib: librawrtc.}
  ##  de-referenced
##
##  TODO
##  rawrtc_data_channel_parameters_get_negotiated
##  rawrtc_data_channel_parameters_get_id
##
##
##  Create data channel options.
##
##  - `deliver_partially`: Enable this if you want to receive partial
##    messages. Disable if messages should arrive complete. If enabled,
##    message chunks will be delivered until the message is complete.
##    Other messages' chunks WILL NOT be interleaved on the same channel.
##

proc rawrtc_data_channel_options_create*(optionsp: ptr ptr rawrtc_data_channel_options;
                                        deliver_partially: bool): rawrtc_code {.
    cdecl, importc: "rawrtc_data_channel_options_create", dynlib: librawrtc.}
  ##  de-referenced
##
##  Get the corresponding name for a data channel state.
##

proc rawrtc_data_channel_state_to_name*(state: rawrtc_data_channel_state): cstring {.
    cdecl, importc: "rawrtc_data_channel_state_to_name", dynlib: librawrtc.}
##
##  Create a data channel.
##

proc rawrtc_data_channel_create*(channelp: ptr ptr rawrtc_data_channel;
                                transport: ptr rawrtc_data_transport;
                                parameters: ptr rawrtc_data_channel_parameters;
                                options: ptr rawrtc_data_channel_options;
    open_handler: rawrtc_data_channel_open_handler; buffered_amount_low_handler: rawrtc_data_channel_buffered_amount_low_handler;
    error_handler: rawrtc_data_channel_error_handler; close_handler: rawrtc_data_channel_close_handler;
    message_handler: rawrtc_data_channel_message_handler; arg: pointer): rawrtc_code {.
    cdecl, importc: "rawrtc_data_channel_create", dynlib: librawrtc.}
  ##  de-referenced
  ##  referenced
  ##  referenced
  ##  nullable, referenced
  ##  nullable
  ##  nullable
  ##  nullable
  ##  nullable
  ##  nullable
  ##  nullable
##
##  Set the argument of a data channel that is passed to the various
##  handlers.
##

proc rawrtc_data_channel_set_arg*(channel: ptr rawrtc_data_channel; arg: pointer): rawrtc_code {.
    cdecl, importc: "rawrtc_data_channel_set_arg", dynlib: librawrtc.}
  ##  nullable
##
##  Set options on a data channel.
##
##  Note: This function must be called directly after creation of the
##  data channel (either by explicitly creating it or implicitly in form
##  of the data channel handler callback) and before calling any other
##  data channel function.
##

proc rawrtc_data_channel_set_options*(channel: ptr rawrtc_data_channel; options: ptr rawrtc_data_channel_options): rawrtc_code {.
    cdecl, importc: "rawrtc_data_channel_set_options", dynlib: librawrtc.}
  ##  nullable, referenced
##
##  Send data via the data channel.
##

proc rawrtc_data_channel_send*(channel: ptr rawrtc_data_channel; buffer: ptr mbuf;
                              is_binary: bool): rawrtc_code {.cdecl,
    importc: "rawrtc_data_channel_send", dynlib: librawrtc.}
  ##  nullable (if empty message), referenced
##
##  Close the data channel.
##

proc rawrtc_data_channel_close*(channel: ptr rawrtc_data_channel): rawrtc_code {.
    cdecl, importc: "rawrtc_data_channel_close", dynlib: librawrtc.}
##
##  TODO (from RTCDataChannel interface)
##  rawrtc_data_channel_get_transport
##  rawrtc_data_channel_get_ready_state
##  rawrtc_data_channel_get_buffered_amount
##  rawrtc_data_channel_get_buffered_amount_low_threshold
##  rawrtc_data_channel_set_buffered_amount_low_threshold
##
##
##  Unset the handler argument and all handlers of the data channel.
##

proc rawrtc_data_channel_unset_handlers*(channel: ptr rawrtc_data_channel): rawrtc_code {.
    cdecl, importc: "rawrtc_data_channel_unset_handlers", dynlib: librawrtc.}
##
##  Get the data channel's parameters.
##

proc rawrtc_data_channel_get_parameters*(parametersp: ptr ptr rawrtc_data_channel_parameters;
                                        channel: ptr rawrtc_data_channel): rawrtc_code {.
    cdecl, importc: "rawrtc_data_channel_get_parameters", dynlib: librawrtc.}
  ##  de-referenced
##
##  Set the data channel's open handler.
##

proc rawrtc_data_channel_set_open_handler*(channel: ptr rawrtc_data_channel; open_handler: ptr rawrtc_data_channel_open_handler): rawrtc_code {.
    cdecl, importc: "rawrtc_data_channel_set_open_handler", dynlib: librawrtc.}
  ##  nullable
##
##  Get the data channel's open handler.
##  Returns `RAWRTC_CODE_NO_VALUE` in case no handler has been set.
##

proc rawrtc_data_channel_get_open_handler*(
    open_handlerp: ptr ptr rawrtc_data_channel_open_handler;
    channel: ptr rawrtc_data_channel): rawrtc_code {.cdecl,
    importc: "rawrtc_data_channel_get_open_handler", dynlib: librawrtc.}
  ##  de-referenced
##
##  Set the data channel's buffered amount low handler.
##

proc rawrtc_data_channel_set_buffered_amount_low_handler*(
    channel: ptr rawrtc_data_channel; buffered_amount_low_handler: ptr rawrtc_data_channel_buffered_amount_low_handler): rawrtc_code {.
    cdecl, importc: "rawrtc_data_channel_set_buffered_amount_low_handler",
    dynlib: librawrtc.}
  ##  nullable
##
##  Get the data channel's buffered amount low handler.
##  Returns `RAWRTC_CODE_NO_VALUE` in case no handler has been set.
##

proc rawrtc_data_channel_get_buffered_amount_low_handler*(
    buffered_amount_low_handlerp: ptr ptr rawrtc_data_channel_buffered_amount_low_handler;
    channel: ptr rawrtc_data_channel): rawrtc_code {.cdecl,
    importc: "rawrtc_data_channel_get_buffered_amount_low_handler",
    dynlib: librawrtc.}
  ##  de-referenced
##
##  Set the data channel's error handler.
##

proc rawrtc_data_channel_set_error_handler*(channel: ptr rawrtc_data_channel; error_handler: ptr rawrtc_data_channel_error_handler): rawrtc_code {.
    cdecl, importc: "rawrtc_data_channel_set_error_handler", dynlib: librawrtc.}
  ##  nullable
##
##  Get the data channel's error handler.
##  Returns `RAWRTC_CODE_NO_VALUE` in case no handler has been set.
##

proc rawrtc_data_channel_get_error_handler*(
    error_handlerp: ptr ptr rawrtc_data_channel_error_handler;
    channel: ptr rawrtc_data_channel): rawrtc_code {.cdecl,
    importc: "rawrtc_data_channel_get_error_handler", dynlib: librawrtc.}
  ##  de-referenced
##
##  Set the data channel's close handler.
##

proc rawrtc_data_channel_set_close_handler*(channel: ptr rawrtc_data_channel; close_handler: ptr rawrtc_data_channel_close_handler): rawrtc_code {.
    cdecl, importc: "rawrtc_data_channel_set_close_handler", dynlib: librawrtc.}
  ##  nullable
##
##  Get the data channel's close handler.
##  Returns `RAWRTC_CODE_NO_VALUE` in case no handler has been set.
##

proc rawrtc_data_channel_get_close_handler*(
    close_handlerp: ptr ptr rawrtc_data_channel_close_handler;
    channel: ptr rawrtc_data_channel): rawrtc_code {.cdecl,
    importc: "rawrtc_data_channel_get_close_handler", dynlib: librawrtc.}
  ##  de-referenced
##
##  Set the data channel's message handler.
##

proc rawrtc_data_channel_set_message_handler*(channel: ptr rawrtc_data_channel; message_handler: ptr rawrtc_data_channel_message_handler): rawrtc_code {.
    cdecl, importc: "rawrtc_data_channel_set_message_handler", dynlib: librawrtc.}
  ##  nullable
##
##  Get the data channel's message handler.
##  Returns `RAWRTC_CODE_NO_VALUE` in case no handler has been set.
##

proc rawrtc_data_channel_get_message_handler*(
    message_handlerp: ptr ptr rawrtc_data_channel_message_handler;
    channel: ptr rawrtc_data_channel): rawrtc_code {.cdecl,
    importc: "rawrtc_data_channel_get_message_handler", dynlib: librawrtc.}
  ##  de-referenced
##
##  Get the corresponding name for a signaling state.
##

proc rawrtc_signaling_state_to_name*(state: rawrtc_signaling_state): cstring {.
    cdecl, importc: "rawrtc_signaling_state_to_name", dynlib: librawrtc.}
##
##  Get the corresponding name for a peer connection state.
##

proc rawrtc_peer_connection_state_to_name*(state: rawrtc_peer_connection_state): cstring {.
    cdecl, importc: "rawrtc_peer_connection_state_to_name", dynlib: librawrtc.}
##
##  Create a new peer connection configuration.
##

proc rawrtc_peer_connection_configuration_create*(
    configurationp: ptr ptr rawrtc_peer_connection_configuration;
    gather_policy: rawrtc_ice_gather_policy): rawrtc_code {.cdecl,
    importc: "rawrtc_peer_connection_configuration_create", dynlib: librawrtc.}
  ##  de-referenced
##
##  Add an ICE server to the peer connection configuration.
##

proc rawrtc_peer_connection_configuration_add_ice_server*(
    configuration: ptr rawrtc_peer_connection_configuration; urls: cstringArray;
    n_urls: csize; username: cstring; credential: cstring;
    credential_type: rawrtc_ice_credential_type): rawrtc_code {.cdecl,
    importc: "rawrtc_peer_connection_configuration_add_ice_server",
    dynlib: librawrtc.}
  ##  copied
  ##  nullable, copied
  ##  nullable, copied
##
##  Get ICE servers from the peer connection configuration.
##

proc rawrtc_peer_connection_configuration_get_ice_servers*(
    serversp: ptr ptr rawrtc_ice_servers;
    configuration: ptr rawrtc_peer_connection_configuration): rawrtc_code {.cdecl,
    importc: "rawrtc_peer_connection_configuration_get_ice_servers",
    dynlib: librawrtc.}
  ##  de-referenced
##
##  Add a certificate to the peer connection configuration to be used
##  instead of an ephemerally generated one.
##

proc rawrtc_peer_connection_configuration_add_certificate*(
    configuration: ptr rawrtc_peer_connection_configuration; certificate: ptr rawrtc_certificate): rawrtc_code {.
    cdecl, importc: "rawrtc_peer_connection_configuration_add_certificate",
    dynlib: librawrtc.}
  ##  copied
##
##  Get certificates from the peer connection configuration.
##

proc rawrtc_peer_connection_configuration_get_certificates*(
    certificatesp: ptr ptr rawrtc_certificates;
    configuration: ptr rawrtc_peer_connection_configuration): rawrtc_code {.cdecl,
    importc: "rawrtc_peer_connection_configuration_get_certificates",
    dynlib: librawrtc.}
  ##  de-referenced
##
##  Set whether to use legacy SDP for data channel parameter encoding.
##  Note: Legacy SDP for data channels is on by default due to parsing problems in Chrome.
##

proc rawrtc_peer_connection_configuration_set_sctp_sdp_05*(
    configuration: ptr rawrtc_peer_connection_configuration; on: bool): rawrtc_code {.
    cdecl, importc: "rawrtc_peer_connection_configuration_set_sctp_sdp_05",
    dynlib: librawrtc.}
##
##  Create a description by parsing it from SDP.
##

proc rawrtc_peer_connection_description_create*(
    descriptionp: ptr ptr rawrtc_peer_connection_description;
    `type`: rawrtc_sdp_type; sdp: cstring): rawrtc_code {.cdecl,
    importc: "rawrtc_peer_connection_description_create", dynlib: librawrtc.}
  ##  de-referenced
##
##  Get the SDP type of the description.
##

proc rawrtc_peer_connection_description_get_sdp_type*(typep: ptr rawrtc_sdp_type;
    description: ptr rawrtc_peer_connection_description): rawrtc_code {.cdecl,
    importc: "rawrtc_peer_connection_description_get_sdp_type", dynlib: librawrtc.}
  ##  de-referenced
##
##  Get the SDP of the description.
##  `*sdpp` will be set to a copy of the SDP that must be unreferenced.
##

proc rawrtc_peer_connection_description_get_sdp*(sdpp: cstringArray;
    description: ptr rawrtc_peer_connection_description): rawrtc_code {.cdecl,
    importc: "rawrtc_peer_connection_description_get_sdp", dynlib: librawrtc.}
  ##  de-referenced
##
##  Create a new ICE candidate from SDP.
##
##  Note: This is equivalent to creating an `RTCIceCandidate` from an
##        `RTCIceCandidateInit` instance in the W3C WebRTC
##        specification.
##

proc rawrtc_peer_connection_ice_candidate_create*(
    candidatep: ptr ptr rawrtc_peer_connection_ice_candidate; sdp: cstring;
    mid: cstring; media_line_index: ptr uint8; username_fragment: cstring): rawrtc_code {.
    cdecl, importc: "rawrtc_peer_connection_ice_candidate_create",
    dynlib: librawrtc.}
  ##  de-referenced
  ##  nullable, copied
  ##  nullable, copied
  ##  nullable, copied
##
##  Encode the ICE candidate into SDP.
##  `*sdpp` will be set to a copy of the SDP attribute that must be
##  unreferenced.
##
##  Note: This is equivalent to the `candidate` attribute of the W3C
##        WebRTC specification's `RTCIceCandidateInit`.
##

proc rawrtc_peer_connection_ice_candidate_get_sdp*(sdpp: cstringArray;
    candidate: ptr rawrtc_peer_connection_ice_candidate): rawrtc_code {.cdecl,
    importc: "rawrtc_peer_connection_ice_candidate_get_sdp", dynlib: librawrtc.}
  ##  de-referenced
##
##  Get the media stream identification tag the ICE candidate is
##  associated to.
##  Return `RAWRTC_CODE_NO_VALUE` in case no 'mid' has been set.
##  Otherwise, `RAWRTC_CODE_SUCCESS` will be returned and `*midp* must
##  be unreferenced.
##

proc rawrtc_peer_connection_ice_candidate_get_sdp_mid*(midp: cstringArray;
    candidate: ptr rawrtc_peer_connection_ice_candidate): rawrtc_code {.cdecl,
    importc: "rawrtc_peer_connection_ice_candidate_get_sdp_mid", dynlib: librawrtc.}
  ##  de-referenced
##
##  Get the media stream line index the ICE candidate is associated to.
##  Return `RAWRTC_CODE_NO_VALUE` in case no media line index has been
##  set.
##

proc rawrtc_peer_connection_ice_candidate_get_sdp_media_line_index*(
    media_line_index: ptr uint8;
    candidate: ptr rawrtc_peer_connection_ice_candidate): rawrtc_code {.cdecl,
    importc: "rawrtc_peer_connection_ice_candidate_get_sdp_media_line_index",
    dynlib: librawrtc.}
  ##  de-referenced
##
##  Get the username fragment the ICE candidate is associated to.
##  Return `RAWRTC_CODE_NO_VALUE` in case no username fragment has been
##  set. Otherwise, `RAWRTC_CODE_SUCCESS` will be returned and
##  `*username_fragmentp* must be unreferenced.
##

proc rawrtc_peer_connection_ice_candidate_get_username_fragment*(
    username_fragmentp: cstringArray;
    candidate: ptr rawrtc_peer_connection_ice_candidate): rawrtc_code {.cdecl,
    importc: "rawrtc_peer_connection_ice_candidate_get_username_fragment",
    dynlib: librawrtc.}
  ##  de-referenced
##
##  Get the underlying ORTC ICE candidate from the ICE candidate.
##  `*ortc_candidatep` must be unreferenced.
##

proc rawrtc_peer_connection_ice_candidate_get_ortc_candidate*(
    ortc_candidatep: ptr ptr rawrtc_ice_candidate;
    candidate: ptr rawrtc_peer_connection_ice_candidate): rawrtc_code {.cdecl,
    importc: "rawrtc_peer_connection_ice_candidate_get_ortc_candidate",
    dynlib: librawrtc.}
  ##  de-referenced
##
##  Create a new peer connection.
##

proc rawrtc_peer_connection_create*(connectionp: ptr ptr rawrtc_peer_connection;
    configuration: ptr rawrtc_peer_connection_configuration;
    negotiation_needed_handler: ptr rawrtc_negotiation_needed_handler;
    local_candidate_handler: ptr rawrtc_peer_connection_local_candidate_handler;
    local_candidate_error_handler: ptr rawrtc_peer_connection_local_candidate_error_handler;
    signaling_state_change_handler: ptr rawrtc_signaling_state_change_handler;
    ice_connection_state_change_handler: ptr rawrtc_ice_transport_state_change_handler;
    ice_gathering_state_change_handler: ptr rawrtc_ice_gatherer_state_change_handler;
    connection_state_change_handler: ptr rawrtc_peer_connection_state_change_handler;
    data_channel_handler: ptr rawrtc_data_channel_handler; arg: pointer): rawrtc_code {.
    cdecl, importc: "rawrtc_peer_connection_create", dynlib: librawrtc.}
  ##  de-referenced
  ##  referenced
  ##  nullable
  ##  nullable
  ##  nullable
  ##  nullable
  ##  nullable
  ##  nullable
  ## nullable
  ##  nullable
  ##  nullable
##
##  Close the peer connection. This will stop all underlying transports
##  and results in a final 'closed' state.
##

proc rawrtc_peer_connection_close*(connection: ptr rawrtc_peer_connection): rawrtc_code {.
    cdecl, importc: "rawrtc_peer_connection_close", dynlib: librawrtc.}
##
##  Create an offer.
##

proc rawrtc_peer_connection_create_offer*(
    descriptionp: ptr ptr rawrtc_peer_connection_description;
    connection: ptr rawrtc_peer_connection; ice_restart: bool): rawrtc_code {.cdecl,
    importc: "rawrtc_peer_connection_create_offer", dynlib: librawrtc.}
  ##  de-referenced
##
##  Create an answer.
##

proc rawrtc_peer_connection_create_answer*(
    descriptionp: ptr ptr rawrtc_peer_connection_description;
    connection: ptr rawrtc_peer_connection): rawrtc_code {.cdecl,
    importc: "rawrtc_peer_connection_create_answer", dynlib: librawrtc.}
  ##  de-referenced
##
##  Set and apply the local description.
##

proc rawrtc_peer_connection_set_local_description*(
    connection: ptr rawrtc_peer_connection; description: ptr rawrtc_peer_connection_description): rawrtc_code {.
    cdecl, importc: "rawrtc_peer_connection_set_local_description",
    dynlib: librawrtc.}
  ##  referenced
##
##  Get local description.
##  Returns `RAWRTC_CODE_NO_VALUE` in case no local description has been
##  set. Otherwise, `RAWRTC_CODE_SUCCESS` will be returned and
##  `*descriptionp` must be unreferenced.
##

proc rawrtc_peer_connection_get_local_description*(
    descriptionp: ptr ptr rawrtc_peer_connection_description;
    connection: ptr rawrtc_peer_connection): rawrtc_code {.cdecl,
    importc: "rawrtc_peer_connection_get_local_description", dynlib: librawrtc.}
  ##  de-referenced
##
##  Set and apply the remote description.
##

proc rawrtc_peer_connection_set_remote_description*(
    connection: ptr rawrtc_peer_connection; description: ptr rawrtc_peer_connection_description): rawrtc_code {.
    cdecl, importc: "rawrtc_peer_connection_set_remote_description",
    dynlib: librawrtc.}
  ##  referenced
##
##  Get remote description.
##  Returns `RAWRTC_CODE_NO_VALUE` in case no remote description has been
##  set. Otherwise, `RAWRTC_CODE_SUCCESS` will be returned and
##  `*descriptionp` must be unreferenced.
##

proc rawrtc_peer_connection_get_remote_description*(
    descriptionp: ptr ptr rawrtc_peer_connection_description;
    connection: ptr rawrtc_peer_connection): rawrtc_code {.cdecl,
    importc: "rawrtc_peer_connection_get_remote_description", dynlib: librawrtc.}
  ##  de-referenced
##
##  Add an ICE candidate to the peer connection.
##

proc rawrtc_peer_connection_add_ice_candidate*(
    connection: ptr rawrtc_peer_connection;
    candidate: ptr rawrtc_peer_connection_ice_candidate): rawrtc_code {.cdecl,
    importc: "rawrtc_peer_connection_add_ice_candidate", dynlib: librawrtc.}
##
##  Get the current signalling state of a peer connection.
##

proc rawrtc_peer_connection_get_signaling_state*(
    statep: ptr rawrtc_signaling_state; connection: ptr rawrtc_peer_connection): rawrtc_code {.
    cdecl, importc: "rawrtc_peer_connection_get_signaling_state", dynlib: librawrtc.}
  ##  de-referenced
##
##  Get the current ICE gathering state of a peer connection.
##

proc rawrtc_peer_connection_get_ice_gathering_state*(
    statep: ptr rawrtc_ice_gatherer_state; connection: ptr rawrtc_peer_connection): rawrtc_code {.
    cdecl, importc: "rawrtc_peer_connection_get_ice_gathering_state",
    dynlib: librawrtc.}
  ##  de-referenced
##
##  Get the current ICE connection state of a peer connection.
##

proc rawrtc_peer_connection_get_ice_connection_state*(
    statep: ptr rawrtc_ice_transport_state; connection: ptr rawrtc_peer_connection): rawrtc_code {.
    cdecl, importc: "rawrtc_peer_connection_get_ice_connection_state",
    dynlib: librawrtc.}
  ##  de-referenced
##
##  Get the current (peer) connection state of the peer connection.
##

proc rawrtc_peer_connection_get_connection_state*(
    statep: ptr rawrtc_peer_connection_state;
    connection: ptr rawrtc_peer_connection): rawrtc_code {.cdecl,
    importc: "rawrtc_peer_connection_get_connection_state", dynlib: librawrtc.}
  ##  de-referenced
##
##  Get indication whether the remote peer accepts trickled ICE
##  candidates.
##
##  Returns `RAWRTC_CODE_NO_VALUE` in case no remote description has been
##  set.
##

proc rawrtc_peer_connection_can_trickle_ice_candidates*(
    can_trickle_ice_candidatesp: ptr bool; connection: ptr rawrtc_peer_connection): rawrtc_code {.
    cdecl, importc: "rawrtc_peer_connection_can_trickle_ice_candidates",
    dynlib: librawrtc.}
  ##  de-referenced
##
##  Create a data channel on a peer connection.
##

proc rawrtc_peer_connection_create_data_channel*(
    channelp: ptr ptr rawrtc_data_channel; connection: ptr rawrtc_peer_connection;
    parameters: ptr rawrtc_data_channel_parameters;
    options: ptr rawrtc_data_channel_options;
    open_handler: ptr rawrtc_data_channel_open_handler; buffered_amount_low_handler: ptr rawrtc_data_channel_buffered_amount_low_handler;
    error_handler: ptr rawrtc_data_channel_error_handler;
    close_handler: ptr rawrtc_data_channel_close_handler;
    message_handler: ptr rawrtc_data_channel_message_handler; arg: pointer): rawrtc_code {.
    cdecl, importc: "rawrtc_peer_connection_create_data_channel", dynlib: librawrtc.}
  ##  de-referenced
  ##  referenced
  ##  nullable, referenced
  ##  nullable
  ##  nullable
  ##  nullable
  ##  nullable
  ##  nullable
  ##  nullable
##
##  Unset the handler argument and all handlers of the peer connection.
##

proc rawrtc_peer_connection_unset_handlers*(
    connection: ptr rawrtc_peer_connection): rawrtc_code {.cdecl,
    importc: "rawrtc_peer_connection_unset_handlers", dynlib: librawrtc.}
##
##  Set the peer connection's negotiation needed handler.
##

proc rawrtc_peer_connection_set_negotiation_needed_handler*(
    connection: ptr rawrtc_peer_connection; negotiation_needed_handler: ptr rawrtc_negotiation_needed_handler): rawrtc_code {.
    cdecl, importc: "rawrtc_peer_connection_set_negotiation_needed_handler",
    dynlib: librawrtc.}
  ##  nullable
##
##  Get the peer connection's negotiation needed handler.
##  Returns `RAWRTC_CODE_NO_VALUE` in case no handler has been set.
##

proc rawrtc_peer_connection_get_negotiation_needed_handler*(
    negotiation_needed_handlerp: ptr ptr rawrtc_negotiation_needed_handler;
    connection: ptr rawrtc_peer_connection): rawrtc_code {.cdecl,
    importc: "rawrtc_peer_connection_get_negotiation_needed_handler",
    dynlib: librawrtc.}
  ##  de-referenced
##
##  Set the peer connection's ICE local candidate handler.
##

proc rawrtc_peer_connection_set_local_candidate_handler*(
    connection: ptr rawrtc_peer_connection; local_candidate_handler: ptr rawrtc_peer_connection_local_candidate_handler): rawrtc_code {.
    cdecl, importc: "rawrtc_peer_connection_set_local_candidate_handler",
    dynlib: librawrtc.}
  ##  nullable
##
##  Get the peer connection's ICE local candidate handler.
##  Returns `RAWRTC_CODE_NO_VALUE` in case no handler has been set.
##

proc rawrtc_peer_connection_get_local_candidate_handler*(local_candidate_handlerp: ptr ptr rawrtc_peer_connection_local_candidate_handler;
    connection: ptr rawrtc_peer_connection): rawrtc_code {.cdecl,
    importc: "rawrtc_peer_connection_get_local_candidate_handler",
    dynlib: librawrtc.}
  ##  de-referenced
##
##  Set the peer connection's ICE local candidate error handler.
##

proc rawrtc_peer_connection_set_local_candidate_error_handler*(
    connection: ptr rawrtc_peer_connection; local_candidate_error_handler: ptr rawrtc_peer_connection_local_candidate_error_handler): rawrtc_code {.
    cdecl, importc: "rawrtc_peer_connection_set_local_candidate_error_handler",
    dynlib: librawrtc.}
  ##  nullable
##
##  Get the peer connection's ICE local candidate error handler.
##  Returns `RAWRTC_CODE_NO_VALUE` in case no handler has been set.
##

proc rawrtc_peer_connection_get_local_candidate_error_handler*(
    local_candidate_error_handlerp: ptr ptr rawrtc_peer_connection_local_candidate_error_handler;
    connection: ptr rawrtc_peer_connection): rawrtc_code {.cdecl,
    importc: "rawrtc_peer_connection_get_local_candidate_error_handler",
    dynlib: librawrtc.}
  ##  de-referenced
##
##  Set the peer connection's signaling state change handler.
##

proc rawrtc_peer_connection_set_signaling_state_change_handler*(
    connection: ptr rawrtc_peer_connection; signaling_state_change_handler: ptr rawrtc_signaling_state_change_handler): rawrtc_code {.
    cdecl, importc: "rawrtc_peer_connection_set_signaling_state_change_handler",
    dynlib: librawrtc.}
  ##  nullable
##
##  Get the peer connection's signaling state change handler.
##  Returns `RAWRTC_CODE_NO_VALUE` in case no handler has been set.
##

proc rawrtc_peer_connection_get_signaling_state_change_handler*(
    signaling_state_change_handlerp: ptr ptr rawrtc_signaling_state_change_handler;
    connection: ptr rawrtc_peer_connection): rawrtc_code {.cdecl,
    importc: "rawrtc_peer_connection_get_signaling_state_change_handler",
    dynlib: librawrtc.}
  ##  de-referenced
##
##  Set the peer connection's ice connection state change handler.
##

proc rawrtc_peer_connection_set_ice_connection_state_change_handler*(
    connection: ptr rawrtc_peer_connection; ice_connection_state_change_handler: ptr rawrtc_ice_transport_state_change_handler): rawrtc_code {.
    cdecl,
    importc: "rawrtc_peer_connection_set_ice_connection_state_change_handler",
    dynlib: librawrtc.}
  ##  nullable
##
##  Get the peer connection's ice connection state change handler.
##  Returns `RAWRTC_CODE_NO_VALUE` in case no handler has been set.
##

proc rawrtc_peer_connection_get_ice_connection_state_change_handler*(
    ice_connection_state_change_handlerp: ptr ptr rawrtc_ice_transport_state_change_handler;
    connection: ptr rawrtc_peer_connection): rawrtc_code {.cdecl,
    importc: "rawrtc_peer_connection_get_ice_connection_state_change_handler",
    dynlib: librawrtc.}
  ##  de-referenced
##
##  Set the peer connection's ice gathering state change handler.
##

proc rawrtc_peer_connection_set_ice_gathering_state_change_handler*(
    connection: ptr rawrtc_peer_connection; ice_gathering_state_change_handler: ptr rawrtc_ice_gatherer_state_change_handler): rawrtc_code {.
    cdecl,
    importc: "rawrtc_peer_connection_set_ice_gathering_state_change_handler",
    dynlib: librawrtc.}
  ##  nullable
##
##  Get the peer connection's ice gathering state change handler.
##  Returns `RAWRTC_CODE_NO_VALUE` in case no handler has been set.
##

proc rawrtc_peer_connection_get_ice_gathering_state_change_handler*(
    ice_gathering_state_change_handlerp: ptr ptr rawrtc_ice_gatherer_state_change_handler;
    connection: ptr rawrtc_peer_connection): rawrtc_code {.cdecl,
    importc: "rawrtc_peer_connection_get_ice_gathering_state_change_handler",
    dynlib: librawrtc.}
  ##  de-referenced
##
##  Set the peer connection's (peer) connection state change handler.
##

proc rawrtc_peer_connection_set_connection_state_change_handler*(
    connection: ptr rawrtc_peer_connection; connection_state_change_handler: ptr rawrtc_peer_connection_state_change_handler): rawrtc_code {.
    cdecl, importc: "rawrtc_peer_connection_set_connection_state_change_handler",
    dynlib: librawrtc.}
  ##  nullable
##
##  Get the peer connection's (peer) connection state change handler.
##  Returns `RAWRTC_CODE_NO_VALUE` in case no handler has been set.
##

proc rawrtc_peer_connection_get_connection_state_change_handler*(
    connection_state_change_handlerp: ptr ptr rawrtc_peer_connection_state_change_handler;
    connection: ptr rawrtc_peer_connection): rawrtc_code {.cdecl,
    importc: "rawrtc_peer_connection_get_connection_state_change_handler",
    dynlib: librawrtc.}
  ##  de-referenced
##
##  Set the peer connection's data channel handler.
##

proc rawrtc_peer_connection_set_data_channel_handler*(
    connection: ptr rawrtc_peer_connection; data_channel_handler: ptr rawrtc_data_channel_handler): rawrtc_code {.
    cdecl, importc: "rawrtc_peer_connection_set_data_channel_handler",
    dynlib: librawrtc.}
  ##  nullable
##
##  Get the peer connection's data channel handler.
##  Returns `RAWRTC_CODE_NO_VALUE` in case no handler has been set.
##

proc rawrtc_peer_connection_get_data_channel_handler*(
    data_channel_handlerp: ptr ptr rawrtc_data_channel_handler;
    connection: ptr rawrtc_peer_connection): rawrtc_code {.cdecl,
    importc: "rawrtc_peer_connection_get_data_channel_handler", dynlib: librawrtc.}
  ##  de-referenced
##
##  Translate a rawrtc return code to a string.
##

proc rawrtc_code_to_str*(code: rawrtc_code): cstring {.cdecl,
    importc: "rawrtc_code_to_str", dynlib: librawrtc.}
##
##  Translate an re error to a rawrtc code.
##

proc rawrtc_error_to_code*(code: cint): rawrtc_code {.cdecl,
    importc: "rawrtc_error_to_code", dynlib: librawrtc.}
##
##  Translate an ICE gather policy to str.
##

proc rawrtc_ice_gather_policy_to_str*(policy: rawrtc_ice_gather_policy): cstring {.
    cdecl, importc: "rawrtc_ice_gather_policy_to_str", dynlib: librawrtc.}
##
##  Translate a str to an ICE gather policy (case-insensitive).
##

proc rawrtc_str_to_ice_gather_policy*(policyp: ptr rawrtc_ice_gather_policy;
                                     str: cstring): rawrtc_code {.cdecl,
    importc: "rawrtc_str_to_ice_gather_policy", dynlib: librawrtc.}
  ##  de-referenced
##
##  Translate a protocol to the corresponding IPPROTO_*.
##

proc rawrtc_ice_protocol_to_ipproto*(protocol: rawrtc_ice_protocol): cint {.cdecl,
    importc: "rawrtc_ice_protocol_to_ipproto", dynlib: librawrtc.}
##
##  Translate a IPPROTO_* to the corresponding protocol.
##

proc rawrtc_ipproto_to_ice_protocol*(protocolp: ptr rawrtc_ice_protocol;
                                    ipproto: cint): rawrtc_code {.cdecl,
    importc: "rawrtc_ipproto_to_ice_protocol", dynlib: librawrtc.}
  ##  de-referenced
##
##  Translate an ICE protocol to str.
##

proc rawrtc_ice_protocol_to_str*(protocol: rawrtc_ice_protocol): cstring {.cdecl,
    importc: "rawrtc_ice_protocol_to_str", dynlib: librawrtc.}
##
##  Translate a str to an ICE protocol (case-insensitive).
##

proc rawrtc_str_to_ice_protocol*(protocolp: ptr rawrtc_ice_protocol; str: cstring): rawrtc_code {.
    cdecl, importc: "rawrtc_str_to_ice_protocol", dynlib: librawrtc.}
  ##  de-referenced
##
##  Translate an ICE candidate type to str.
##

proc rawrtc_ice_candidate_type_to_str*(`type`: rawrtc_ice_candidate_type): cstring {.
    cdecl, importc: "rawrtc_ice_candidate_type_to_str", dynlib: librawrtc.}
##
##  Translate a str to an ICE candidate type (case-insensitive).
##

proc rawrtc_str_to_ice_candidate_type*(typep: ptr rawrtc_ice_candidate_type;
                                      str: cstring): rawrtc_code {.cdecl,
    importc: "rawrtc_str_to_ice_candidate_type", dynlib: librawrtc.}
  ##  de-referenced
##
##  Translate an ICE TCP candidate type to str.
##

proc rawrtc_ice_tcp_candidate_type_to_str*(`type`: rawrtc_ice_tcp_candidate_type): cstring {.
    cdecl, importc: "rawrtc_ice_tcp_candidate_type_to_str", dynlib: librawrtc.}
##
##  Translate a str to an ICE TCP candidate type (case-insensitive).
##

proc rawrtc_str_to_ice_tcp_candidate_type*(
    typep: ptr rawrtc_ice_tcp_candidate_type; str: cstring): rawrtc_code {.cdecl,
    importc: "rawrtc_str_to_ice_tcp_candidate_type", dynlib: librawrtc.}
  ##  de-referenced
##
##  Translate an ICE role to str.
##

proc rawrtc_ice_role_to_str*(role: rawrtc_ice_role): cstring {.cdecl,
    importc: "rawrtc_ice_role_to_str", dynlib: librawrtc.}
##
##  Translate a str to an ICE role (case-insensitive).
##

proc rawrtc_str_to_ice_role*(rolep: ptr rawrtc_ice_role; str: cstring): rawrtc_code {.
    cdecl, importc: "rawrtc_str_to_ice_role", dynlib: librawrtc.}
  ##  de-referenced
##
##  Translate a DTLS role to str.
##

proc rawrtc_dtls_role_to_str*(role: rawrtc_dtls_role): cstring {.cdecl,
    importc: "rawrtc_dtls_role_to_str", dynlib: librawrtc.}
##
##  Translate a str to a DTLS role (case-insensitive).
##

proc rawrtc_str_to_dtls_role*(rolep: ptr rawrtc_dtls_role; str: cstring): rawrtc_code {.
    cdecl, importc: "rawrtc_str_to_dtls_role", dynlib: librawrtc.}
  ##  de-referenced
##
##  Translate a certificate sign algorithm to str.
##

proc rawrtc_certificate_sign_algorithm_to_str*(
    algorithm: rawrtc_certificate_sign_algorithm): cstring {.cdecl,
    importc: "rawrtc_certificate_sign_algorithm_to_str", dynlib: librawrtc.}
##
##  Translate a str to a certificate sign algorithm (case-insensitive).
##

proc rawrtc_str_to_certificate_sign_algorithm*(
    algorithmp: ptr rawrtc_certificate_sign_algorithm; str: cstring): rawrtc_code {.
    cdecl, importc: "rawrtc_str_to_certificate_sign_algorithm", dynlib: librawrtc.}
  ##  de-referenced
##
##  Translate an SDP type to str.
##

proc rawrtc_sdp_type_to_str*(`type`: rawrtc_sdp_type): cstring {.cdecl,
    importc: "rawrtc_sdp_type_to_str", dynlib: librawrtc.}
##
##  Translate a str to an SDP type.
##

proc rawrtc_str_to_sdp_type*(typep: ptr rawrtc_sdp_type; str: cstring): rawrtc_code {.
    cdecl, importc: "rawrtc_str_to_sdp_type", dynlib: librawrtc.}
  ##  de-referenced
##
##  Duplicate a string.
##

proc rawrtc_strdup*(destinationp: cstringArray; source: cstring): rawrtc_code {.cdecl,
    importc: "rawrtc_strdup", dynlib: librawrtc.}
##
##  Print a formatted string to a buffer.
##

proc rawrtc_snprintf*(destinationp: cstring; size: csize; formatter: cstring): rawrtc_code {.
    varargs, cdecl, importc: "rawrtc_snprintf", dynlib: librawrtc.}
##
##  Print a formatted string to a dynamically allocated buffer.
##

proc rawrtc_sdprintf*(destinationp: cstringArray; formatter: cstring): rawrtc_code {.
    varargs, cdecl, importc: "rawrtc_sdprintf", dynlib: librawrtc.}
