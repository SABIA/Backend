## *
##  @file re_mbuf.h  Interface to memory buffers
## 
##  Copyright (C) 2010 Creytiv.com
## 

when not defined(RELEASE):
  const
    MBUF_DEBUG* = 1

## * Defines a memory buffer

type
  mbuf* {.bycopy.} = object
    buf*: ptr uint8           ## *< Buffer memory
    size*: csize               ## *< Size of buffer
    pos*: csize                ## *< Position in buffer
    `end`*: csize              ## *< End of buffer
  
  pl* {.bycopy.} = object
  
  re_printf* {.bycopy.} = object
  

proc mbuf_alloc*(size: csize): ptr mbuf {.importc: "mbuf_alloc", dynlib: "librawrtc.so".}
proc mbuf_alloc_ref*(mbr: ptr mbuf): ptr mbuf {.importc: "mbuf_alloc_ref",
    dynlib: "librawrtc.so".}
proc mbuf_init*(mb: ptr mbuf) {.importc: "mbuf_init", dynlib: "librawrtc.so".}
proc mbuf_reset*(mb: ptr mbuf) {.importc: "mbuf_reset", dynlib: "librawrtc.so".}
proc mbuf_resize*(mb: ptr mbuf; size: csize): cint {.importc: "mbuf_resize",
    dynlib: "librawrtc.so".}
proc mbuf_trim*(mb: ptr mbuf) {.importc: "mbuf_trim", dynlib: "librawrtc.so".}
#proc mbuf_shift*(mb: ptr mbuf; shift: ssize_t): cint {.importc: "mbuf_shift",
#    dynlib: "librawrtc.so".}
proc mbuf_write_mem*(mb: ptr mbuf; buf: ptr uint8; size: csize): cint {.
    importc: "mbuf_write_mem", dynlib: "librawrtc.so".}
proc mbuf_write_u8*(mb: ptr mbuf; v: uint8): cint {.importc: "mbuf_write_u8",
    dynlib: "librawrtc.so".}
proc mbuf_write_u16*(mb: ptr mbuf; v: uint16): cint {.importc: "mbuf_write_u16",
    dynlib: "librawrtc.so".}
proc mbuf_write_u32*(mb: ptr mbuf; v: uint32): cint {.importc: "mbuf_write_u32",
    dynlib: "librawrtc.so".}
proc mbuf_write_u64*(mb: ptr mbuf; v: uint64): cint {.importc: "mbuf_write_u64",
    dynlib: "librawrtc.so".}
proc mbuf_write_str*(mb: ptr mbuf; str: cstring): cint {.importc: "mbuf_write_str",
    dynlib: "librawrtc.so".}
proc mbuf_write_pl*(mb: ptr mbuf; pl: ptr pl): cint {.importc: "mbuf_write_pl",
    dynlib: "librawrtc.so".}
proc mbuf_read_mem*(mb: ptr mbuf; buf: ptr uint8; size: csize): cint {.
    importc: "mbuf_read_mem", dynlib: "librawrtc.so".}
proc mbuf_read_u8*(mb: ptr mbuf): uint8 {.importc: "mbuf_read_u8",
                                       dynlib: "librawrtc.so".}
proc mbuf_read_u16*(mb: ptr mbuf): uint16 {.importc: "mbuf_read_u16",
    dynlib: "librawrtc.so".}
proc mbuf_read_u32*(mb: ptr mbuf): uint32 {.importc: "mbuf_read_u32",
    dynlib: "librawrtc.so".}
proc mbuf_read_u64*(mb: ptr mbuf): uint64 {.importc: "mbuf_read_u64",
    dynlib: "librawrtc.so".}
proc mbuf_read_str*(mb: ptr mbuf; str: cstring; size: csize): cint {.
    importc: "mbuf_read_str", dynlib: "librawrtc.so".}
proc mbuf_strdup*(mb: ptr mbuf; strp: cstringArray; len: csize): cint {.
    importc: "mbuf_strdup", dynlib: "librawrtc.so".}
#proc mbuf_vprintf*(mb: ptr mbuf; fmt: cstring; ap: va_list): cint {.
#    importc: "mbuf_vprintf", dynlib: "librawrtc.so".}
proc mbuf_printf*(mb: ptr mbuf; fmt: cstring): cint {.varargs, importc: "mbuf_printf",
    dynlib: "librawrtc.so".}
proc mbuf_write_pl_skip*(mb: ptr mbuf; pl: ptr pl; skip: ptr pl): cint {.
    importc: "mbuf_write_pl_skip", dynlib: "librawrtc.so".}
proc mbuf_fill*(mb: ptr mbuf; c: uint8; n: csize): cint {.importc: "mbuf_fill",
    dynlib: "librawrtc.so".}
proc mbuf_debug*(pf: ptr re_printf; mb: ptr mbuf): cint {.importc: "mbuf_debug",
    dynlib: "librawrtc.so".}
## *
##  Get the buffer from the current position
## 
##  @param mb Memory buffer
## 
##  @return Current buffer
## 

#proc mbuf_buf*(mb: ptr mbuf): ptr uint8 {.inline.} =
#  return if not isNil(mb):
#     (mb.buf[] + mb.pos)
#    else:
#     cast[ptr uint8](nil)

## *
##  Get number of bytes left in a memory buffer, from current position to end
## 
##  @param mb Memory buffer
## 
##  @return Number of bytes left
## 

proc mbuf_get_left*(mb: ptr mbuf): csize {.inline.} =
  return if (not isNil(mb) and (mb.`end` > mb.pos)):
     (mb.`end` - mb.pos)
    else: 0

## *
##  Get available space in buffer (size - pos)
## 
##  @param mb Memory buffer
## 
##  @return Number of bytes available in buffer
## 

proc mbuf_get_space*(mb: ptr mbuf): csize {.inline.} =
  return if (not isNil(mb) and (mb.size > mb.pos)):
     (mb.size - mb.pos) 
    else: 0

## *
##  Set absolute position
## 
##  @param mb  Memory buffer
##  @param pos Position
## 

proc mbuf_set_pos*(mb: ptr mbuf; pos: csize) {.inline.} =
  mb.pos = pos
  #MBUF_CHECK_POS(mb)

## *
##  Set absolute end
## 
##  @param mb  Memory buffer
##  @param end End position
## 

proc mbuf_set_end*(mb: ptr mbuf; `end`: csize) {.inline.} =
  mb.`end` = `end`
  #MBUF_CHECK_END(mb)

## *
##  Advance position +/- N bytes
## 
##  @param mb  Memory buffer
##  @param n   Number of bytes to advance
## 

proc mbuf_advance*(mb: ptr mbuf; n: csize) {.inline.} =
  inc(mb.pos, n)
  #MBUF_CHECK_POS(mb)

## *
##  Rewind position and end to the beginning of buffer
## 
##  @param mb  Memory buffer
## 

proc mbuf_rewind*(mb: ptr mbuf) {.inline.} =
  mb.`end` = 0
  mb.pos = 0

## *
##  Set position to the end of the buffer
## 
##  @param mb  Memory buffer
## 

proc mbuf_skip_to_end*(mb: ptr mbuf) {.inline.} =
  mb.pos = mb.`end`
