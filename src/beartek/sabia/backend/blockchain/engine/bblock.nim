# Copyright 2017 Yoshihiro Tanaka
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

  # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Author: Yoshihiro Tanaka <contact@cordea.jp>
# date  : 2018-02-03

import times
import sha256

type
  Block*[T] = ref BlockObj[T]
  BlockObj[T] = object
    depth*: int
    timestamp: float
    data*: T
    hash*: string
    previousHash*: string

proc getCalcString[T](depth: int, timestamp: float, data: T, prevHash: string): string =
  result = $depth & $timestamp & $data & prevHash

proc calcHash[T](blk: Block[T]): string =
  result = calcHash(getCalcString(blk.depth, blk.timestamp, blk.data, blk.previousHash))

proc newBlock*[T](depth: int, data: T, prevHash: string): Block[T] =
  let timestamp = epochTime()
  result = Block[T](
    depth: depth,
    timestamp: timestamp,
    data: data,
    hash: calcHash(getCalcString(depth, timestamp, data, prevHash)),
    previousHash: prevHash
  )

proc getGenesisBlock*[T](genesis_data: T): Block[T] =
  result = Block[T](
    depth: 0,
    timestamp: 1518413918.591908,
    data: genesis_data,
    hash: calcHash(getCalcString(0, 1518413918.591908, genesis_data, "0")),
    previousHash: "0"
  )

proc isValid*[T](blk: Block[T], prev: Block): bool =
  if blk.depth - 1 != prev.depth:
    return false
  elif blk.previousHash != prev.hash:
    return false
  elif calcHash(blk) != blk.hash:
    return false
  return true
