# Copyright 2017 Yoshihiro Tanaka
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

  # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software # distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Author: Yoshihiro Tanaka <contact@cordea.jp>
# date  : 2018-02-04

import websocket
import asynchttpserver
import asyncnet
import asyncdispatch
import marshal
import sequtils
import bblock
import message
import messagebuilder
import blockmanager

type
  Wshandler*[T] = object
    manager*: BlockManager[T]
    clients: seq[AsyncSocket]

const
  RequestAll = "requestall"
  ResponseAll = "responseall"
  RequestLatest = "requestlatest"
  ResponseLatest = "responselatest"

var async_handler = Wshandler[string](
          clients: @[],
          manager: newBlockManager(" ")
          )
proc sethandler*[T](handler: var Wshandler[T]) =
  async_handler = handler

proc newWshandler*[T](genesis_data: T): Wshandler[T] =
  result = Wshandler[T](
            clients: @[],
            manager: newBlockManager(genesis_data)
            )

proc broadcast*[T](handler: Wshandler[T], data: string) =
  for client in handler.clients:
    waitFor client.sendText(data, false)

proc getLatestRequest*[T](): string =
  echo "Request all blocks"
  result = newMessageBuilder[T]()
    .code(RequestLatest)
    .build()
    .toJson()

proc getLatestResponse*[T](handler: var Wshandler[T]): string =
  echo "Request latest block"
  let latest = handler.manager.latestBlock()
  result = newMessageBuilder[T]()
    .code(ResponseLatest)
    .blocks(@[latest])
    .build()
    .toJson()

proc onReceivedLatestResponse[T](handler: var Wshandler[T], data: seq[Block]) =
  let latest = handler.manager.latestBlock()
  if latest.depth < data[0].depth:
    if latest.hash == data[0].previousHash:
      echo "Got the latest block"
      handler.manager.blocks.add(data[0])
    else:
      handler.broadcast(
        newMessageBuilder[T]()
          .code(RequestAll)
          .build()
          .toJson()
      )

proc onReceivedAllResponse[T](handler: var Wshandler[T], data: seq[Block]) =
  handler.manager.replace(data)

proc call*[T](sock: AsyncSocket) {.async.} =
  var handler = async_handler
  await sock.sendText(getLatestRequest[T](), false)
  while true:
    try:
      let f = await sock.readData(false)
      if f.opcode == Opcode.Text:
        let msg = to[Message[T]](f.data)
        case msg.code
        of RequestLatest:
          echo "Received request of latest block"
          waitFor sock.sendText(handler.getLatestResponse(), false)
        of RequestAll:
          echo "Received request of all blocks"
          let response = newMessageBuilder[T]()
            .code(ResponseAll)
            .blocks(handler.manager.blocks)
            .build()
            .toJson()
          waitFor sock.sendText(response, false)
        of ResponseAll:
          echo "Received all blocks"
          handler.onReceivedAllResponse(msg.blocks)
        of ResponseLatest:
          echo "Received latest block"
          handler.onReceivedLatestResponse(msg.blocks)
    except:
      echo getCurrentExceptionMsg()
      # clients.delete(req.client)
      break

proc initWs*[T](number: int) {.async.} =
  proc cb(req: Request) {.async,gcsafe.} =
    let (success, error) = await verifyWebsocketRequest(req)
    if success:
      async_handler.clients.add(req.client)
      asyncCheck call[T](req.client)
    let server = newAsyncHttpServer()
    asyncCheck server.serve(Port(number), cb)

proc connect*[T](address: string, number: int) {.async.} =
  echo "Connect to " & address
  let ws = await newAsyncWebsocket(address, Port number, "/?encoding=text", ssl = false)
  async_handler.clients.add(ws.sock)
  asyncCheck call[T](ws.sock)
