[Package]
name          = "bearengine"
version       = "0.0.0"
author        = "endes"
description   = "Blockchain engine"
license       = "Apache 2.0"

[Deps]
Requires: "nimSHA2 >= 0.1.1"
