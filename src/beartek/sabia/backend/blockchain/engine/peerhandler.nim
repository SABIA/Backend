# Copyright 2017 Yoshihiro Tanaka
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

  # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software # distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Author: Yoshihiro Tanaka <contact@cordea.jp>
# date  : 2018-02-04

import rawrtc/rawrtc
import rawrtc/re_main
import rawrtc/re_mbuf
import sequtils
import bblock
import message
import messagebuilder
import blockmanager
import morelogging

import marshal
import typetraits
import typeinfo
import threadpool
import tables

type
  Peerhandler*[T] = object
    manager*: BlockManager[T]
    #ice_candidate_types*: seq[string]
    gather_options*: ptr rawrtc_ice_gather_options
    role*: rawrtc_ice_role
    certificate*: ptr rawrtc_certificate
    gatherer*: ptr rawrtc_ice_gatherer
    ice_transport*: ptr rawrtc_ice_transport
    dtls_transport*: ptr rawrtc_dtls_transport
    sctp_transport*: ptr rawrtc_sctp_transport
    data_transport*: ptr rawrtc_data_transport
    channel*: ptr rawrtc_data_channel
    data_channel_negotiated*: ref data_channel_helper
  data_channel_helper* {.bycopy.} = object
    #le*: le
    channel*: ptr rawrtc_data_channel
    label*: string
    #client*: ptr client
    arg*: pointer

  ice_candidate = object
     foundation: string
     protocol: rawrtc_ice_protocol
     priority: uint32
     ip: string
     r_ip: string
     port: uint16
     r_port: uint16
     `type`: rawrtc_ice_candidate_type
     tcp_type: rawrtc_ice_tcp_candidate_type
  stcp_parm = object
    capabilities*: ptr rawrtc_sctp_capabilities
    port*: uint16
  peer_parameters = object
    ice: ptr rawrtc_ice_parameters
    candidates: seq[ice_candidate]
    dtls: ptr rawrtc_dtls_parameters
    stcp: stcp_parm

const
  RequestAll = "requestall"
  ResponseAll = "responseall"
  RequestLatest = "requestlatest"
  ResponseLatest = "responselatest"

let logger = newAsyncFileLogger()

proc checkerr(msg: rawrtc_code, pos: tuple[filename: string, line: int]) =
  if $msg == $RAWRTC_CODE_SUCCESS:
    logger.debug(msg, " on ", pos.filename, ":", $pos.line)
  else:
    logger.error(msg, " on ", pos.filename, ":", $pos.line)

template log(msg: rawrtc_code) =
  let pos = instantiationInfo()
  checkerr(msg, pos)

proc create_str_buf(str: string) : ptr mbuf =
  var vstr = str
  var buf = mbuf_alloc(size(toAny(vstr)))
  discard mbuf_write_str(buf, str)
  return buf

proc default_ice_gatherer_error_handler[T](candidate: ptr rawrtc_ice_candidate; url: cstring; error_code: uint16; error_text: cstring; arg: pointer) {.cdecl.} =
  var client = cast[ptr Peerhandler[T]](arg)
  echo("ICE gatherer error, URL:", url, ", reason: ", error_text)

proc default_ice_gatherer_local_candidate_handler[T](candidate: ptr rawrtc_ice_candidate; url: cstring; arg: pointer) {.cdecl.} =
  var client = cast[ptr Peerhandler[T]](arg)
  if not isNil(candidate):
    echo repr(candidate)
    var ip: cstring
    var port: uint16
    var `type`: rawrtc_ice_candidate_type

    ##  Get candidate information
    log(rawrtc_ice_candidate_get_ip(addr(ip), candidate))
    log(rawrtc_ice_candidate_get_port(addr(port), candidate))
    log(rawrtc_ice_candidate_get_type(addr(`type`), candidate))

    logger.info("New Ice candidate: " & $`type` & " " & $ip & ":" & $port)

proc default_ice_gatherer_state_change_handler[T](state: rawrtc_ice_gatherer_state; arg: pointer) {.cdecl.} =
  var client = cast[ptr Peerhandler[T]](arg)
  var state_name: cstring = rawrtc_ice_gatherer_state_to_name(state)
  echo(" ICE gatherer state:", state_name)

  if state_name == "complete":
    echo($$client[].get_parameters())

proc default_ice_transport_state_change_handler[T](state: rawrtc_ice_transport_state; arg: pointer) {.cdecl.} =
  var client = cast[ptr Peerhandler[T]](arg)
  var state_name: cstring = rawrtc_ice_transport_state_to_name(state)
  echo(" ICE transport state:", state_name)

proc default_ice_transport_candidate_pair_change_handler[T](local: ptr rawrtc_ice_candidate; remote: ptr rawrtc_ice_candidate; arg: pointer) {.cdecl.} =
  var client = cast[ptr Peerhandler[T]](arg)
  echo(" ICE transport candidate pair change")

proc dtls_transport_state_change_handler[T](state: rawrtc_dtls_transport_state; arg: pointer) {.cdecl.} =
  ##  read-only
  var client = cast[ptr Peerhandler[T]](arg)

  echo(rawrtc_dtls_transport_state_to_name(state))

proc default_dtls_transport_error_handler[T](arg: pointer) {.cdecl.} =
  ##  TODO: error.message (probably from OpenSSL)
  ##  will be casted to `struct client*`
  var client = cast[ptr Peerhandler[T]](arg)

  echo("DTLS transport error: ", "???")

proc default_sctp_transport_state_change_handler[T](state: rawrtc_sctp_transport_state; arg: pointer) {.cdecl.} =
  ##  read-only
  var client = cast[ptr Peerhandler[T]](arg)

  echo(rawrtc_sctp_transport_state_to_name(state))

proc default_data_channel_handler[T](channel: ptr rawrtc_data_channel; arg: pointer) {.cdecl.} =
  var client = cast[ptr Peerhandler[T]](arg)
  var parameters: ptr rawrtc_data_channel_parameters


  log(rawrtc_data_channel_get_parameters(addr(parameters), channel))
  log(rawrtc_data_channel_parameters_get_label(allocCStringArray([]), parameters))
  echo(" New data channel instance: ", "n/a")

proc newPeerhandler*[T](genesis_data: T): Peerhandler[T] =
  var gather_options: ptr rawrtc_ice_gather_options
  const google_ice = ["stun:stun.l.google.com:19302","stun:stun1.l.google.com:19302"]
  const threema_ice = ["turn:turn.threema.ch:443"]

  log(rawrtc_init())

  log( rawrtc_ice_gather_options_create(addr(gather_options), RAWRTC_ICE_GATHER_POLICY_ALL))
  ##  Add ICE servers to ICE gather options
  log( rawrtc_ice_gather_options_add_server(gather_options, allocCStringArray(google_ice), google_ice.len(), nil, nil, RAWRTC_ICE_CREDENTIAL_TYPE_NONE))
  log( rawrtc_ice_gather_options_add_server(gather_options, allocCStringArray(threema_ice), threema_ice.len(), "threema-angular", "Uv0LcCq3kyx6EiRwQW5jVigkhzbp70CjN2CJqzmRxG3UGIdJHSJV6tpo7Gj7YnGB", RAWRTC_ICE_CREDENTIAL_TYPE_PASSWORD))

  result = Peerhandler[T](
            #clients: @[],
            gather_options: gather_options,
            role: RAWRTC_ICE_ROLE_CONTROLLING,
            manager: newBlockManager(genesis_data),
            )

  ##  Generate certificates
  log( rawrtc_certificate_generate(addr(result.certificate), nil))
  ##  Create ICE gatherer
  log( rawrtc_ice_gatherer_create(addr(result.gatherer), result.gather_options,
                                 default_ice_gatherer_state_change_handler[T],
                                 default_ice_gatherer_error_handler[T],
                                 default_ice_gatherer_local_candidate_handler[T], addr(result)))
  ##  Create ICE transport
  log( rawrtc_ice_transport_create(addr(result.ice_transport), result.gatherer,
                                  default_ice_transport_state_change_handler[T],
                                  default_ice_transport_candidate_pair_change_handler[T],
                                  addr(result)))
  ##  Create DTLS transport
  log( rawrtc_dtls_transport_create(addr(result.dtls_transport),
                                   result.ice_transport, [result.certificate],
                                   1,
                                   dtls_transport_state_change_handler[T],
                                   default_dtls_transport_error_handler[T], addr(result)))
  ##  Create SCTP transport
  log( rawrtc_sctp_transport_create(addr(result.sctp_transport),
                                   result.dtls_transport, 5000,
                                   default_data_channel_handler[T],
                                   default_sctp_transport_state_change_handler[T],
                                   addr(result)))
  ##  Get data transport
  log(rawrtc_sctp_transport_get_data_transport(addr(result.data_transport),
      result.sctp_transport))

  ##  Create data channel helper
  var helper = new(data_channel_helper)
  #helper.client = addr(result)
  helper.label = "bearengine"
  result.data_channel_negotiated = helper

proc onReceivedAllResponse[T](handler: var Peerhandler[T], data: seq[Block]) =
  handler.manager.replace(data)

proc get_parameters*[T](handler: var Peerhandler[T]) : peer_parameters =
  var ice : ptr rawrtc_ice_parameters
  var candidates : ptr rawrtc_ice_candidates
  var dtls : ptr rawrtc_dtls_parameters
  var cap : ptr rawrtc_sctp_capabilities

  log(rawrtc_ice_gatherer_get_local_parameters(addr(ice), handler.gatherer))
  log(rawrtc_ice_gatherer_get_local_candidates(addr(candidates), handler.gatherer))
  log(rawrtc_dtls_transport_get_local_parameters(addr(dtls), handler.dtls_transport))
  log(rawrtc_sctp_transport_get_capabilities(addr(cap)))

  var cand_seq : seq[ice_candidate] = @[]
  for n in 0 .. candidates.n_candidates-1:
    var candidate = candidates.candidates[n]
    var foundation: cstring
    var protocol: rawrtc_ice_protocol
    var priority: uint32
    var ip: cstring
    var r_ip: cstring
    var port: uint16
    var r_port: uint16
    var `type`: rawrtc_ice_candidate_type
    var tcp_type: rawrtc_ice_tcp_candidate_type


    #  Get candidate information
    log(rawrtc_ice_candidate_get_foundation(addr(foundation), candidate))
    log(rawrtc_ice_candidate_get_protocol(addr(protocol), candidate))
    log(rawrtc_ice_candidate_get_priority(addr(priority), candidate))
    log(rawrtc_ice_candidate_get_ip(addr(ip), candidate))
    log(rawrtc_ice_candidate_get_port(addr(port), candidate))
    log(rawrtc_ice_candidate_get_type(addr(`type`), candidate))
    log(rawrtc_ice_candidate_get_tcp_type(addr(tcp_type), candidate))
    log(rawrtc_ice_candidate_get_related_address(addr(r_ip), candidate))
    log(rawrtc_ice_candidate_get_related_port(addr(r_port), candidate))

    cand_seq.add(ice_candidate(
                   foundation: $foundation,
                   protocol: protocol,
                   priority: priority,
                   ip: $ip,
                   r_ip: $r_ip,
                   port: port,
                   r_port: r_port,
                   `type`: `type`,
                   tcp_type: tcp_type))

  result = peer_parameters(
            ice: ice,
            candidates: cand_seq,
            dtls: dtls,
            stcp: stcp_parm(capabilities: cap, port: 5000)
           )

proc from_parameters*[T](handler: Peerhandler[T], params: peer_parameters) =
  #rawrtc_ice_candidate_create
  #rawrtc_ice_transport_add_remote_candidate
  for cand in params.candidates:
    var rawcand: ptr rawrtc_ice_candidate

    log(rawrtc_ice_candidate_create(addr(rawcand), cand.foundation, cand.priority, cand.ip, cand.protocol, cand.port, cand.`type`, cand.tcp_type, cand.r_ip, cand.r_port))
    log(rawrtc_ice_transport_add_remote_candidate(handler.ice_transport, rawcand))
  log(rawrtc_ice_transport_start(handler.ice_transport, handler.gatherer, params.ice, handler.role))
  log(rawrtc_dtls_transport_start(handler.dtls_transport, params.dtls))
  log(rawrtc_sctp_transport_start(handler.sctp_transport, params.stcp.capabilities, params.stcp.port))

proc parameters_to_JSON*(params: peer_parameters) : string =
  result = $$params

proc parameters_from_JSON*(json: string) : peer_parameters =
  result = to[peer_parameters](json)


proc broadcast*[T](handler: Peerhandler[T], data: string) = discard
#  for client in handler.clients:
#    waitFor client.sendText(data, false)

proc getLatestRequest*[T](): string =
  echo "Request all blocks"
  result = newMessageBuilder[T]()
    .code(RequestLatest)
    .build()
    .toJson()

proc getLatestResponse*[T](handler: var Peerhandler[T]): string =
  echo "Request latest block"
  let latest = handler.manager.latestBlock()
  result = newMessageBuilder[T]()
    .code(ResponseLatest)
    .blocks(@[latest])
    .build()
    .toJson()

proc onReceivedLatestResponse[T](handler: var Peerhandler[T], data: seq[Block]) =
  let latest = handler.manager.latestBlock()
  if latest.depth < data[0].depth:
    if latest.hash == data[0].previousHash:
      echo "Got the latest block"
      handler.manager.blocks.add(data[0])
    else:
      handler.broadcast(
        newMessageBuilder[T]()
          .code(RequestAll)
          .build()
          .toJson()
      )

proc default_data_channel_error_handler(arg: pointer) {.cdecl.} =
  var channel: ptr data_channel_helper = cast[ptr data_channel_helper](arg)
  #var client: ptr client = channel.client
  echo("(", ") Data channel error: ", channel.label)

proc default_data_channel_close_handler(arg: pointer) {.cdecl.} =
  var channel: ptr data_channel_helper = cast[ptr data_channel_helper](arg)
  #var client: ptr client = channel.client
  echo("(", ") Data channel close: ", channel.label)

proc default_data_channel_message_handler[T](buffer: ptr mbuf; flags: rawrtc_data_channel_message_flag; arg: pointer) {.cdecl.} =
  var handler = cast[ptr Peerhandler[T]](arg)
  #var client: ptr client = channel.client
  var data : cstring
  discard buffer.mbuf_read_str(data, buffer.size)
  
  let msg = to[Message[T]]($data)
  case msg.code
  of RequestLatest:
    echo "Received request of latest block"
    log(rawrtc_data_channel_send(handler.channel, create_str_buf(handler[].getLatestResponse()), false))
  of RequestAll:
    echo "Received request of all blocks"
    let response = newMessageBuilder[T]()
      .code(ResponseAll)
      .blocks(handler.manager.blocks)
      .build()
      .toJson()
    log(rawrtc_data_channel_send(handler.channel, create_str_buf(response), false))
  of ResponseAll:
    echo "Received all blocks"
    handler[].onReceivedAllResponse(msg.blocks)
  of ResponseLatest:
    echo "Received latest block"
    handler[].onReceivedLatestResponse(msg.blocks)

proc default_data_channel_buffered_amount_low_handler(arg: pointer) {.cdecl.} =
  var channel = cast[ptr data_channel_helper](arg)
  #var client: ptr client = channel.client
  echo(" Data channel buffered amount low: %s\x0A", channel.label)

proc data_channel_open_handler(arg: pointer) {.cdecl.} =
  var channel = cast[ptr data_channel_helper](arg)
  #var client: ptr client = channel.client
  echo(" Data channel open: %s\x0A", channel.label)

proc sig(sig:cint) =
  re_cancel()

proc initRawThread() =
  echo(re_main(sig))

proc initPeer*[T](handler: var Peerhandler[T]) =
  #proc cb(req: Request) {.async,gcsafe.} =
  #  let (success, error) = await verifyWebsocketRequest(req)
  #  if success:
  #    async_handler.clients.add(req.client)
  #    asyncCheck call[T](req.client)
  var channel_parameters: ptr rawrtc_data_channel_parameters

  ##  Create data channel parameters
  log(rawrtc_data_channel_parameters_create(addr(channel_parameters), "bearengine", RAWRTC_DATA_CHANNEL_TYPE_RELIABLE_ORDERED, 0, nil, true, 0))

  ##  Create pre-negotiated data channel
  log(rawrtc_data_channel_create(addr(handler.channel),
                               handler.data_transport, channel_parameters, nil,
                               data_channel_open_handler, default_data_channel_buffered_amount_low_handler,
                               default_data_channel_error_handler,
                               default_data_channel_close_handler,
                               default_data_channel_message_handler[T],
                               addr(handler)))

  handler.connect()
  spawn initRawThread()

proc connect*[T](handler: var Peerhandler[T]) =
#  echo "Connect to " & address
#  let ws = await newAsyncWebsocket(address, Port number, "/?encoding=text", ssl = false)
#  async_handler.clients.add(ws.sock)
#  asyncCheck call[T](ws.sock)
  log( rawrtc_ice_gatherer_gather(handler.gatherer, nil))
