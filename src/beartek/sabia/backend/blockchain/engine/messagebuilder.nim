# Copyright 2017 Yoshihiro Tanaka
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

  # http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Author: Yoshihiro Tanaka <contact@cordea.jp>
# date  : 2018-02-12

import bblock
import message
import marshal

type
  MessageBuilder[T] = object
    code: string
    blocks: seq[Block[T]]

proc newMessageBuilder*[T](): MessageBuilder[T] =
  result = MessageBuilder[T](
    code: "",
    blocks: @[]
  )

proc code*[T](builder: MessageBuilder[T], code: string): MessageBuilder[T] =
  result = builder
  result.code = code

proc blocks*[T](builder: MessageBuilder[T], blocks: seq[Block[T]]): MessageBuilder[T] =
  result = builder
  result.blocks = blocks

proc build*[T](builder: MessageBuilder[T]): Message[T] =
  result = newMessage(builder.code, builder.blocks)

proc toJson*[T](message: Message[T]): string =
  result = $$message
